﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SqlGrid {
    public partial class Form1 : Form {
        SqlStorage SqlStorage { get; set; }
        DataGridViewRowCollection AllRows { get; set; }
        //DataGridViewRowCollection VisibleRows { get; set; }
        public Form1(SqlStorage sqlStorage) {
            SqlStorage = sqlStorage;
            InitializeComponent();
        }

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            //DataGridView1.DataSource = SqlStorage.DataSet.Tables[SqlStorage.TableName];
            //AllRows = DataGridView1.Rows;
        }
        private void DataGridView1_Update() {
            for(int i = 0; i < AllRows.Count; i++) {
                DataGridViewRow gridViewRow = AllRows[i];
                if(dateTimePicker1.Value < (DateTime)gridViewRow.Cells[1].Value && dateTimePicker2.Value > (DateTime)gridViewRow.Cells[1].Value) {
                    gridViewRow.Visible = true;
                } else {
                    gridViewRow.Visible = false;
                }

            }
        }
        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e) {
            //if(dateTimePicker2.Value > dateTimePicker1.Value){
            //    DataGridView1_Update();
            //}
        }
        private void dateTimePicker2_ValueChanged(object sender, EventArgs e) {
            //if(dateTimePicker2.Value > dateTimePicker1.Value) {
            //    DataGridView1_Update();
            //}
        }
        private void label1_Click(object sender, EventArgs e) {

        }

        private void label2_Click(object sender, EventArgs e) {

        }
    }
}
