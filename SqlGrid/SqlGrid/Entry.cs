using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SqlGrid {
    static class Entry {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            IConfigurationRoot config = GetConfig();
            string connectionString = config.GetConnectionString("myLocalConnection");
            string tableName = config.GetSection("Tables")["TableMain"];
            SqlStorage sqlStorage = new SqlStorage(connectionString);
            sqlStorage.DownloadTable(tableName);
            Application.Run(new Form1(sqlStorage));
        }

        static IConfigurationRoot GetConfig() {
            string projectDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            IConfigurationBuilder builder = new ConfigurationBuilder().AddJsonFile(projectDirectory + @"\appsettings.json");
            return builder.Build();
        }
    }
}
