﻿using System;
using System.Windows.Forms;
using System.IO;
using OfficeOpenXml;
using System.Text;
using System.Linq;
using System.Data;
using System.ComponentModel;

namespace SqlGrid2 {
    public partial class Form1 : Form {
        SqlStorage SqlStorage { get; set; }
        DataGridViewRowCollection AllRows { get; set; }
        BindingSource bindingSource = new BindingSource();
        BindingList<DataTable> tables;
        int RowsCount;
        int pageLength = 23;
        public Form1(SqlStorage sqlStorage) {
            SqlStorage = sqlStorage;
            InitializeComponent();
        }

        public Form1() {
            InitializeComponent();
        }
        private void UpdateGrid(Func<DataRow, bool> compare) {
            tables = new BindingList<DataTable>();
            int counter = 0;
            DataTable dataTable = new DataTable();
            dataTable = SqlStorage.DataSet.Tables[SqlStorage.TableName].Clone();
            foreach(DataRow row in SqlStorage.DataSet.Tables[SqlStorage.TableName].Rows) {
                if(counter % pageLength == 0 && dataTable.Rows.Count > 0) {
                    tables.Add(dataTable);
                    dataTable = SqlStorage.DataSet.Tables[SqlStorage.TableName].Clone();
                }
                if(compare(row)) {
                    dataTable.Rows.Add(row.ItemArray);
                    counter++;
                }
            }
            RowsCount = counter;
            if(tables.Count == 0) {
                tables.Add(dataTable = SqlStorage.DataSet.Tables[SqlStorage.TableName].Clone());
            }
            bindingNavigator1.BindingSource = bindingSource;
            bindingSource.DataSource = tables;
            bindingSource.PositionChanged += bs_PositionChanged;
            bs_PositionChanged(bindingSource, EventArgs.Empty);
        }
        private void Form1_Load(object sender, EventArgs e) {
            UpdateGrid(row => true);
        }
        void bs_PositionChanged(object sender, EventArgs e) {
            DataGridView1.DataSource = tables[bindingSource.Position];
        }
        private void DataGridView1_Update() {
            UpdateGrid(row => dateTimePicker1.Value.Date <= (DateTime)row["Date"] && dateTimePicker2.Value.Date >= (DateTime)row["Date"]);
        }
        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e) {
            if(dateTimePicker2.Value.Date >= dateTimePicker1.Value.Date) {
                DataGridView1_Update();
            }
        }
        private void dateTimePicker2_ValueChanged(object sender, EventArgs e) {
            if(dateTimePicker2.Value.Date >= dateTimePicker1.Value.Date) {
                DataGridView1_Update();
            }
        }
        private void label1_Click(object sender, EventArgs e) {

        }
        private void label2_Click(object sender, EventArgs e) {

        }
        private void button1_Click(object sender, EventArgs e) {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Excel Files|*.xlsx;*.xls;*.xlsm";
            if(saveFileDialog1.ShowDialog() == DialogResult.OK) {
                SaveInXls(saveFileDialog1.FileName);
            }
        }
        private void button2_Click(object sender, EventArgs e) {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "CSV file (*.csv)|*.csv| All Files (*.*)|*.*";
            if(saveFileDialog1.ShowDialog() == DialogResult.OK) {
                SaveInCsv(saveFileDialog1.FileName);
            }
        }
        private void SaveInXls(string filePath) {
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            using(var xlsx = new ExcelPackage(new FileInfo(filePath))) {
                var sheet = xlsx.Workbook.Worksheets.Add(DateTime.Now.ToString());
                for(int m = 0; m < DataGridView1.Columns.Count; m++) {
                    sheet.Cells[1, m + 1].Value = DataGridView1.Columns[m].HeaderText;
                }
                int i = 2;
                foreach(var table in tables) {
                    foreach(DataRow row in table.Rows) {
                        int j = 1;
                        foreach(var item in row.ItemArray) {
                            var cell = sheet.Cells[i, j];
                            if(item is DateTime) {
                                DateTime dateTime = (DateTime)item;
                                cell.Value = dateTime.ToOADate();
                                cell.Style.Numberformat.Format = "mm-dd-yy";
                            } else {
                                cell.Value = item;
                            }
                            j++;
                        }
                        i++;
                    }
                }
                xlsx.Save();
            }
        }
        private void SaveInCsv(string filePath) {
            string[] csvRows = new string[RowsCount + 1];
            string[] columnHeaders = new string[DataGridView1.Columns.Count];
            for(int j = 0; j < DataGridView1.Columns.Count; j++) {
                columnHeaders[j] = DataGridView1.Columns[j].HeaderText;
            }
            csvRows[0] = String.Join(",", columnHeaders);
            int i = 1;
            foreach(var table in tables) {
                foreach(DataRow row in table.Rows) {
                    csvRows[i] = String.Join(",", row.ItemArray);
                    i++;
                }
            }
            File.WriteAllLines(filePath, csvRows, Encoding.UTF8);
        }

        private void bindingNavigator1_RefreshItems(object sender, EventArgs e) {

        }

        private void bindingNavigatorPositionItem_Click(object sender, EventArgs e) {

        }
    }
}