﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlGrid2 {
    public  class SqlStorage {
        public DataSet DataSet{ get; private set; }
        public string TableName { get; set; }
        string ConnectionString { get; set; }

        public SqlStorage(string connectionString) {
            ConnectionString = connectionString;
            DataSet = new DataSet();
        }
        public void DownloadTable(string tableName){
            using(SqlConnection connection = new SqlConnection(ConnectionString)) {
                SqlCommand sqlCommand = new SqlCommand($"Select * from {tableName}", connection);
                connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                 while(!sqlDataReader.IsClosed){
                    DataSet.Tables.Add(tableName).Load(sqlDataReader);
                }
                TableName = tableName;
            }
            //
            for(int i = 0; i < 10000; i++) {
                var row = DataSet.Tables[TableName].NewRow();
                row.ItemArray = DataSet.Tables[TableName].Rows[0].ItemArray;
                DataSet.Tables[TableName].Rows.Add(row);
            }
        }
    }
}