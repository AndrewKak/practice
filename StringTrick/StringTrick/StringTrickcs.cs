﻿using System;
namespace StringTrick {
    class StringTrick {
        unsafe static void ChangePointer(string current) {
            string requiredOutput = "ssssss";
            char[] array = requiredOutput.ToCharArray(0, requiredOutput.Length);
            fixed(char* charPointerT = current) {
                for(int i = 0; i < requiredOutput.Length; i++) {
                    charPointerT[i] = array[i];
                }
            }
        }
        static void Main() {
            string prepare = "test";
            ChangePointer(prepare);
            var output = "test";
            Console.WriteLine(output);
            CompareStrings(prepare, output);
        }
        static bool CompareStrings(string one, string two) {
            return Object.ReferenceEquals(one, two);
        }
    }
}

