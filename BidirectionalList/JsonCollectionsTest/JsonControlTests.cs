using BidirectionalList;
using DatabaseLocal;
using NUnit.Framework;
using System;
using System.IO;
using UserItem;
namespace JsonCollectionsTest {
    public class CollectionJsonTests {
        const int LENGTH = 91;
        string wrongLocation = "|||users.json";
        string storageLocation = AppDomain.CurrentDomain.BaseDirectory + "users.json";
        string storageLocation2 = @"D:\users.json";
        string webReference = "https://northwind.netcore.io/customers.json";
        JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> baseList;
        JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> baseList2;
        DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>> databaseEditor;
        DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>> databaseEditor2;
        public JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> CreateJsonMethod(string storageLocaition, string webReference) {
            return new JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>>(storageLocaition, webReference);
        }
        public DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>> GetDatabaseEditor(JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> jsonControl) {
            return new DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>>(jsonControl);
        }
        [SetUp]
        public void Setup() {
            File.Delete(storageLocation);
            File.Delete(storageLocation2);
            baseList2 = CreateJsonMethod(storageLocation, webReference);
            baseList = CreateJsonMethod(storageLocation2, webReference);
            databaseEditor = GetDatabaseEditor(baseList);
            databaseEditor2 = GetDatabaseEditor(baseList2);
        }
        [Test]
        public void ConstructorTest() {
            Assert.Throws<DirectoryNotFoundException>(() => CreateJsonMethod(string.Empty, string.Empty));
            Assert.Throws<UriFormatException>(() => CreateJsonMethod(storageLocation, string.Empty));
            Assert.DoesNotThrow(() => CreateJsonMethod(storageLocation, webReference));
        }
        [Test]
        public void DownloadTest() {
            baseList.DownloadData();
            Assert.IsTrue(databaseEditor.Length() == LENGTH);
            baseList2.DownloadData();
            Assert.IsTrue( databaseEditor2.Length() == LENGTH);
            baseList.DownloadData();
            Assert.IsTrue(databaseEditor.Length() == LENGTH);
            baseList2.DownloadData();
            Assert.IsTrue(databaseEditor2.Length() == LENGTH);
            Assert.Throws<DirectoryNotFoundException>(() => CreateJsonMethod(wrongLocation, webReference).DownloadData());
            Assert.Throws<UriFormatException>(() => CreateJsonMethod(storageLocation, String.Empty).DownloadData());
        }
        [Test]
        public void ReadDatabaseLocalTest() {
            baseList.ReadDatabaseLocal();
            Assert.IsTrue( databaseEditor.Length() == LENGTH );
            databaseEditor2.Add(new User());
            baseList2.ReadDatabaseLocal();
            baseList2.DownloadData();
            Assert.IsTrue(databaseEditor2.Length() == LENGTH);
            databaseEditor.ChangeStorage(wrongLocation);
            Assert.Throws<DirectoryNotFoundException>(() => baseList.ReadDatabaseLocal());
        }
        [Test]
        public void SaveCollectionTest() {
            databaseEditor.Add(new User());
            Assert.DoesNotThrow(()=>baseList.SaveCollection());
            baseList.ReadDatabaseLocal();
            Assert.IsTrue((databaseEditor.Length() == LENGTH + 1));
            databaseEditor.ChangeStorage(wrongLocation);
            Assert.Throws<DirectoryNotFoundException>(() => baseList.SaveCollection());
        }
    }
}