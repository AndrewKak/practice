﻿using BidirectionalList;
using DatabaseLocal;
using System;
using System.Net;
using System.IO;
using UserItem;
using NUnit.Framework;
using Newtonsoft.Json;

namespace JsonCollectionsTest {
    class RemoteJsonTests {
        const int LENGTH = 91;
        string wrongLocation = "|||users.json";
        User userSample = new User();
        string storageLocation = AppDomain.CurrentDomain.BaseDirectory + "users.json";
        string storageLocation2 = @"D:\users.json";
        string webReference = "https://northwind.netcore.io/customers.json";
        JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> baseList;
        JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> baseList2;
        DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>> databaseEditor;
        DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>> databaseEditor2;
        public bool ContainSample(DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>> database) {
            foreach(var item in database.GetCurrentCollection()) {
                if(item.Equals(userSample)) {
                    return true;
                }
            }
            return false;
        }
        public JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> CreateJsonMethod(string storageLocaition, string webReference) {
            return new JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>>(storageLocaition, webReference);
        }
        public DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>> GetDatabaseEditor(JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> jsonControl) {
            return new DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>>(jsonControl);
        }
        [SetUp]
        public void Setup() {
            File.Delete(storageLocation);
            File.Delete(storageLocation2);
            baseList2 = CreateJsonMethod(storageLocation, webReference);
            baseList = CreateJsonMethod(storageLocation2, webReference);
            databaseEditor = GetDatabaseEditor(baseList);
            databaseEditor2 = GetDatabaseEditor(baseList2);
            userSample.id = "AKAK";
        }
        [Test]
        public void CheckRefereceTest() {
            databaseEditor.ChangeRemoteSource("https://northwind.netcore.io/query/customers.json?CountryStartsWith=U");
            Assert.DoesNotThrow(() => databaseEditor.CheckRemoteSource());
            databaseEditor.ChangeRemoteSource("https://njkgfjhgk=U");
            Assert.Throws<UriFormatException>(() => databaseEditor.CheckRemoteSource());
        }
        [Test]
        public void DeserializeRemoteTest() { 
            databaseEditor.ChangeRemoteSource("https://northwind.netcore.io/query/customers.json?CountryStartsWith=U");
            Assert.DoesNotThrow(() => databaseEditor.DeserializeRemote());
        }
    }
}