﻿using System;
namespace BidirectionalList {
    public interface IBidirectionaList<T> {
        Type ReturnContentType();
        void AddFirst(T value);
        void Add(T value);
        void AddAfter(T searchValue, T value);
        void AddAtPosition(int index, T value);
        T FindByIndex(int index);
        T FindByValue(T value);
        int FindIndexByValue(T value);
        T FindByCondition(Func<T, bool> condition);
        void DeleteByIndex(int index);
        void DeleteByValue(T value);
        void DeleteByCondition(Func<T, bool> condition);
    }
}