﻿namespace BidirectionalList {
    public  interface IContainCollection<TCollection>   {
        TCollection GetCollection();
    }
}