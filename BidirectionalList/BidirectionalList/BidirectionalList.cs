﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace BidirectionalList {
    internal class ItemList<T> {
        public ItemList<T> Previous { get; set; }
        public ItemList<T> Next { get; set; }
        public T Value { get; set; }
    }
    public class BidirectionalList<T> : ICollection<T>, IContainCollection<BidirectionalList<T>>, IBidirectionaList<T> where T : IEquatable<T>{
        ItemList<T> firstItem; 
        ItemList<T> lastItem;
        public int Length { get; private set; }

        public int Count => Length;

        public bool IsReadOnly => false;
        public BidirectionalList() {
        }
        public BidirectionalList(T[] array) {
            if(array == null || array.Length == 0) {
                Length = 0;
            } else {
                AddFirst(array[0]);
                Length = 1;
                for(int i = 1; i < array.Length; i++) {
                    Add(array[i]);
                }
            }
        }
        public BidirectionalList<T> GetCollection() {
            return this;
        }
        public void Clear() {
            firstItem = null;
            lastItem = null;
            Length = 0;
        }
        public bool Contains(T item) {
            return FindByValue(item) != null;
        }
        public void CopyTo(T[] array, int arrayIndex) {
            ItemList<T> item = firstItem;
            while(item != null) {
                array[arrayIndex] = item.Value;
                arrayIndex++;
                item = item.Next;
            }
        }
        public bool Remove(T item) {
            T foundedItem = FindByValue(item);
            if(foundedItem != null) {
                DeleteByIndex(FindIndexByValue(foundedItem));
                return true;
            } else {
                return false;
            }
        }
        public IEnumerable<T> GetValues() {
            //for(int i = 0; i < Length; i++) {
            //    yield return FindByIndex(i);
            //}
            var item = firstItem;
            while(item != null) {
                yield return item.Value;
                item = item.Next;
            }
        }
        public IEnumerator<T> GetEnumerator() {
            return GetValues().GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
        public Type ReturnContentType() {
            ItemList<T> item = new ItemList<T>();
            return item.GetType().GetProperty("Value").PropertyType;
        }
        public void AddFirst(T value) {
            AddItem(null, firstItem, value);
        }
        public void Add(T value) {
            AddItem(lastItem, null, value);
        }
        public void AddAfter(T searchValue, T value) {
            ItemList<T> item = ItemByValue(searchValue, out _);
            if(item != null) {
                AddItem(item, item.Next, value);
            } else {
                throw new ElementNotFoundException();
            }
        }
        public void AddAtPosition(int index, T value) {
            if(index != Length) {
                ItemList<T> item = ItemByIndex(index);
                AddItem(item.Previous, item, value);
            } else if(Length > 0) {
                AddItem(lastItem, null, value);
            } else {
                AddItem(value);
            }
        }
        public void DeleteByIndex(int index) {
            IndexRangeCheck(index);
            ItemList<T> item = ItemByIndex(index);
            DeleteItem(item);
        }
        public void DeleteByValue(T value) {
            ItemList<T> item = ItemByValue(value, out _);
            while(item != null) {
                DeleteItem(item);
                item = ItemByValue(value, out _);
            }
        }
        public void DeleteByCondition(Func<T, bool> condition) {
            ItemList<T> item = ItemByCondition(condition);
            while(item != null) {
                DeleteItem(item);
                item = ItemByCondition(condition);
            }
        }
        public T FindByIndex(int index) {
            ItemList<T> item = ItemByIndex(index);
            if(item != null) {
                return item.Value;
            } else {
                throw new IndexOutOfRangeException();
            }

        }
        public T FindByValue(T value) {
            ItemList<T> item = ItemByValue(value, out _);
            return item.Value;
        }
        public int FindIndexByValue(T value) {
            int currentIndex;
            ItemList<T> item = ItemByValue(value,out currentIndex);
            ItemNullCheck(item);
            return currentIndex;
        }
        public T FindByCondition(Func<T, bool> func) {
            ItemList<T> item = ItemByCondition(func);
            return ItemNullCheck(item);
        }
        void AddItem(ItemList<T> previous, ItemList<T> next, T value) {
            ItemList<T> item = new ItemList<T>();
            if(previous != null) {
                previous.Next = item;
                item.Previous = previous;
            } else {
                firstItem = item;
            }
            if(next != null) {
                next.Previous = item;
                item.Next = next;
            } else {
                lastItem = item;
            }
            Length++;
            item.Value = value;
        }
        void AddItem(T value) {
            AddItem(null, null, value);
        }
        ItemList<T> ItemByIndex(int index) {
            IndexRangeCheck(index);
            ItemList<T> item = firstItem;
            for(int i = 0; i < index; i++) {
                item = item.Next;
            }
            return item;
        }
        ItemList<T> ItemByValue(T value, out int currentIndex) {
            ItemList<T> item = firstItem;
            T valueObject = value;
            currentIndex = 0;
            while(item != null) {
                T foundedObject = item.Value;
                if(valueObject.Equals(foundedObject)) {
                    return item;
                }
                currentIndex++;
                item = item.Next;
            }
            return null;
        }
        ItemList<T> ItemByCondition(Func<T, bool> func) {
            ItemList<T> item = firstItem;
            if(func != null) {
                while(item != null) {
                    if(func(item.Value)) {
                        return item;
                    }
                    item = item.Next;
                }
            } else {
                throw new ArgumentNullException();
            }
            return null;
        }
        void DeleteItem(ItemList<T> item) {
            if(item != null) {
                if(item.Next != null) {
                    item.Next.Previous = item.Previous;
                }
                if(item.Previous != null) {
                    item.Previous.Next = item.Next;
                } else {
                    firstItem = item.Next;
                }
                Length--;
            }
        }
        static T ItemNullCheck(ItemList<T> item) {
            if(item != null) {
                return item.Value;
            } else {
                throw new ElementNotFoundException();
            }
        }
        void IndexRangeCheck(int index) {
            if(index > Length || index < 0) {
                throw new IndexOutOfRangeException();
            }
        }
    }
}