﻿using System;
using System.Data;
using Microsoft.EntityFrameworkCore;
//using LinqToDB.Mapping;

namespace CarSqlData {
  //  [Table(Name = "cars")]
    public class CarInfo : IEquatable<CarInfo> {
       // [PrimaryKey, Identity]
        public string Car { get; set; }

        //[Column(Name = "MPG")]
        public double MPG { get; set; }
      //  [Column(Name = "Cylinders")]
        public int Cylinders { get; set; }
      //  [Column(Name = "Displacement")]
        public double Displacement { get; set; }
      //  [Column(Name = "Horsepower")]
        public double Horsepower { get; set; }
     //   [Column(Name = "Weight")]
        public double Weight { get; set; }
     //   [Column(Name = "Acceleration")]
        public double Acceleration { get; set; }
      //  [Column(Name = "Model")]
        public double Model { get; set; }
     //   [Column(Name = "Origin")]
        public string Origin { get; set; }

        public CarInfo(){ 
        }

        public CarInfo(string car, double mPG, int cylinders, double displacement, double horsepower, double weight, double acceleration, double model, string origin) {
            Car = car;
            MPG = mPG;
            Cylinders = cylinders;
            Displacement = displacement;
            Horsepower = horsepower;
            Weight = weight;
            Acceleration = acceleration;
            Model = model;
            Origin = origin;
        }

        //public CarInfo(DataRow row) {
        //    Car = row["Car"].ToString(); 
        //    MPG = Convert.ToDouble(row["MPG"]);
        //    Cylinders = Convert.ToInt32(row["Cylinders"]);
        //    Displacement = Convert.ToDouble(row["Displacement"]);
        //    Horsepower = Convert.ToDouble(row["Horsepower"]);
        //    Weight = Convert.ToDouble(row["Weight"]);
        //    Acceleration = Convert.ToDouble(row["Acceleration"]);
        //    Model = Convert.ToSingle(row["Model"]);
        //    Origin = row["Origin"].ToString();
        //}
        public bool Equals(CarInfo other) => (Car.CompareTo(other.Car)==0) && MPG == other.MPG && Cylinders == other.Cylinders && Displacement == other.Displacement && Horsepower == other.Horsepower && Weight == other.Weight && Acceleration == other.Acceleration && Model == other.Model && Origin == other.Origin;
        public override int GetHashCode() {
            int hash = 2;
            hash = (hash * 7) + (!Object.ReferenceEquals(null, Car) ? Car.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, MPG) ? MPG.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Cylinders) ? Cylinders.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Displacement) ? Displacement.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Horsepower) ? Horsepower.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Weight) ? Weight.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Acceleration) ? Acceleration.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Model) ? Weight.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.ReferenceEquals(null, Origin) ? Acceleration.GetHashCode() : 0);
            return hash;
        }
    }
}
