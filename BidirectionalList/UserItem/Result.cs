﻿using BidirectionalList;
namespace UserItem {
    public class Result<TCollection> : IContainCollection<TCollection> {
        public TCollection Results { get; set; }
        public TCollection GetCollection() {
            return Results;
        }
    }
}