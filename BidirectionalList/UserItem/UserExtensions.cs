﻿using BidirectionalList;
using DatabaseLocal;
namespace UserItem {
    public static class UserExtensions {
        public static void Add(this User user, JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> database) {
            DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>> databaseEditor = new DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>>(database);
            databaseEditor.Add(user);
        }
        public static void Remove(this User user, JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> database) {
            DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>> databaseEditor = new DatabaseEditor<BidirectionalList<User>, User, Result<BidirectionalList<User>>>(database);
            databaseEditor.Remove(user);
        }
    }
}