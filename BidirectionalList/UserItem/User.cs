﻿using System;
namespace UserItem {
    public partial class User : IEquatable<User> {
        public string id { get; set; }
        public string companyName { get; set; }
        public string contactName { get; set; }
        public string contactTitle { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
#nullable enable
        public string? fax { get; set; }
        public string? region { get; set; }
#nullable disable
        public override bool Equals(object other) => other is User otherCar && Equals(otherCar);
        public bool Equals(User other) => StringEqualCheck(other) && postalCode == other.postalCode;
        public override int GetHashCode() {
            int hash = 2;
            hash = (hash * 7) + (!Object.ReferenceEquals(null, id) ? id.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.ReferenceEquals(null, companyName) ? companyName.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, contactName) ? contactName.GetHashCode() : 0);
            return hash;
        }
        bool StringEqualCheck(User other) {
            return String.Compare(id, other.id) == 0 && String.Compare(companyName, other.companyName) == 0 && String.Compare(contactName, other.contactName) == 0 && String.Compare(contactTitle, other.contactTitle) == 0 && String.Compare(address, other.address) == 0 && String.Compare(city, other.city) == 0 && String.Compare(country, other.country) == 0 && String.Compare(phone, other.phone) == 0 && String.Compare(fax, other.fax) == 0;
        }
    }
}