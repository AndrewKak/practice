﻿using System;
using BidirectionalList;
using DatabaseLocal;
namespace ParseJsonFromURL {
    public class Result<TCollection> : IContainCollection<TCollection> {
        public TCollection Results { get; set; }
        public TCollection GetCollection() {
            return Results;
        }        
    }
}