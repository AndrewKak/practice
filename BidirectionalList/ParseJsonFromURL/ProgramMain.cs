﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using BidirectionalList;
using CarSqlData;
using DatabaseLocal;
using LinqToDB;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;

namespace ParseJsonFromURL {
    static class ProgramMain {
        static void Main() {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "users.json";
            User user = new User();
            user.address = "123";
            user.city = "123";
            //IConfigurationRoot configuration = GetConfig();
            //JsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> bidirectionalListJson = new JsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>>(filePath, webReference);
            //JsonControl<List<User>, User, Result<List<User>>> listJson = new JsonControl<List<User>, User, Result<List<User>>>(filePath, webReference);
            //user.Add(bidirectionalListJson);
            //user.Remove(bidirectionalListJson);
            //Console.ReadKey();
            //SqlXmlControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> baseList = new SqlXmlControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>>(AppDomain.CurrentDomain.BaseDirectory + "users.xml", configuration.GetConnectionString("myConnectionString"));
            string connection = GetConfig().GetConnectionString("myConnectionString");
            //Serialize(connection);
            var data = Execute("SELECT * FROM dbo.cars; ", connection);
            var data1 = Execute("SELECT * FROM dbo.cars WHERE Origin = 'Japan'; ", connection);
            var data2 = Execute("DELETE FROM dbo.cars WHERE Origin = 'Japan'; ", connection);
             CarInfo car = new CarInfo("Toyota",1,1,1,1,1,1,1,"Japan");
            CarInfo updateCar = new CarInfo("Nissan", 2, 2, 2, 2, 2, 2, 2, "Japan");
            for(int i = 0; i < 100; i++) {
                var data3 = Execute($"INSERT INTO cars (Car, MPG, Cylinders, Displacement, Horsepower, Weight, Acceleration, Model, Origin) VALUES('{car.Car}', '{car.MPG}', '{car.Cylinders}', '{car.Displacement}',  '{car.Weight}','{car.Horsepower}', '{car.Acceleration}', '{car.Model}', '{car.Origin}' ); ", connection);
            }
            var data5 = Execute("UPDATE cars SET Car = 'Nissan' WHERE Origin = 'Japan'; ", connection);
            var data4 = Execute("SELECT * FROM dbo.cars WHERE Origin = 'Japan'; ", connection);
            var data6 = Execute("DELETE FROM dbo.cars WHERE Car = 'Nissan'; ", connection);
        }
        static IConfigurationRoot GetConfig() {
            string projectDirectory = Environment.CurrentDirectory;
            IConfigurationBuilder builder = new ConfigurationBuilder().AddJsonFile(projectDirectory + @"\jsconfig.json");
            return builder.Build();
        }
        //public static void Serialize(string connection) {
        //    CarInfo car = new CarInfo("aaa", 0, 0, 0, 0, 00, 0, 0, "ASDASD");
        //    using(var db = new DbLocal(connection)) {
        //        //var query = from p in db.CarInfos.Take();
        //    }
        //}
        private static DataTable Execute(string commandText, string connectionString) {
            DataTable dataTable = new DataTable();
            using(SqlConnection connection = new SqlConnection(connectionString)) {
                using(SqlCommand command = new SqlCommand()) {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;
                    connection.Open();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                    dataAdapter.Fill(dataTable);
                    return dataTable;
                }
            }
        }
        //public class DbLocal : LinqToDB.Data.DataConnection {
        //    public DbLocal(string storageLocation) : base("SqlServer", storageLocation) { }

        //    public ITable<CarInfo> CarInfos => GetTable<CarInfo>();
        //}
    }

}