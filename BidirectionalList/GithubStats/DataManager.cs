﻿using Microsoft.Extensions.Configuration;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Timers;

namespace GithubStats {
    public class DataManager<T1, T2, T3> {
        Requester Requester { get; set; }
        DataStorage DataStorage { get; set; }
        IConfigurationRoot Configuration { get; set; }
        int UpdateTimes { get; set; }
        Timer Execute { get; set; }
        string TextLocation { get; set; }
        public DataManager(string orgName, string productName) {
            IConfigurationRoot Configuration = GetConfig();
            string connectionString = Configuration.GetConnectionString("RemoteString");
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
            string database = builder.InitialCatalog;
            DataStorage = new MsSqlStorage(connectionString, database);
            Requester = new GithubRequester(orgName, productName, Configuration.GetSection("Tokens")["GitTokenReserve"], DataStorage);
            UpdateTimes = Int32.Parse(Configuration.GetSection("Update")["TimesPerDay"]);
            TextLocation = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\log.txt";
        }
        public void Start() {
            TimeSpan now = TimeSpan.Parse(DateTime.Now.ToString("HH:mm:ss"));
            TimeSpan activationTime = new TimeSpan(0, 1440 / UpdateTimes, 0);
            while(activationTime.TotalMinutes <= now.TotalMinutes) {
                activationTime += new TimeSpan(0, 1440 / UpdateTimes, 0);
            }
            TimeSpan timeLeft = (activationTime - now);
            Execute = new Timer();
            Execute.Interval = timeLeft.TotalMilliseconds;
            Execute.Elapsed += OnUpdateEvent;
            Execute.Start();
            Console.WriteLine($"Update planned on {activationTime}.");
            WriteInLog($"Update planned on {activationTime}.");
            Console.ReadKey();
        }
        void WriteInLog(string record){
            using(var streamWriter = new StreamWriter(TextLocation,true)) {
                streamWriter.WriteLine(record);
            }
        } 
        IConfigurationRoot GetConfig() {
            string projectDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            IConfigurationBuilder builder = new ConfigurationBuilder().AddJsonFile(projectDirectory + @"\appsettings.json");
            return builder.Build();
        }
        void OnUpdateEvent(object sender, ElapsedEventArgs eArgs) {
            Execute.Enabled = false;
            try {
                Requester.DownloadInfo();
                Console.WriteLine($"Update succeded on {DateTime.Now}.");
                WriteInLog($"Update succeded on {DateTime.Now}.");
            } catch(Exception e) {
                if(e is SqlException) {
                    Console.WriteLine($"SqlError {DateTime.Now}. {e}");
                    WriteInLog($"SqlError {DateTime.Now}. {e}");
                    throw;
                }
            }
            Start();
        }
    }
}