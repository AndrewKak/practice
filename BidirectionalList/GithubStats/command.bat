dotnet build "GithubStats.csproj" -c Release -o /app
SCHTASKS /CREATE  /TN "GithubStats" /TR "\"%~dp0bin\Release\net5.0\GithubStats.exe"" /SC ONSTART
dotnet run "GithubStats.csproj" -c Release -o /app