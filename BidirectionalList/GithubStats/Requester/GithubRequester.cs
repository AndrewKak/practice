﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace GithubStats {
    class GithubRequester : Requester {
        public GithubRequester(string orgName, string productName, string token, DataStorage dataStorage) : base(orgName, productName, token, dataStorage) {
            SetRepos();
        }
        protected internal override void SetRepos() {
            var response = GetResponse($"https://api.github.com/orgs/{OrgName}/repos").Result;
            string json;
            using(var strm = new StreamReader(response.GetResponseStream())) {
                json = strm.ReadToEnd();
            }
            JArray obj = JArray.Parse(json);
            Repositories = new List<Repository>();
            foreach(var item in obj) {
                Repository repository = new Repository();
                Repositories.Add(repository);
                repository.name = item.Value<string>("name");
            }
            return;
        }
        protected internal override void AddInfo<T>(Repository repo) {
            var response = GetResponse($"https://api.github.com/repos/ReposSpawn/{repo.name}/traffic/{typeof(T).Name.ToLower()}").Result;
            string json;
            using(var strm = new StreamReader(response.GetResponseStream())) {
                json = strm.ReadToEnd();
            }
            T info = JsonConvert.DeserializeObject<T>(json);
            SetProperty(repo, typeof(T).Name.ToLower(), info);
        }
        private Task<WebResponse> GetResponse(string uri) {
            try{
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                req.Method = "GET";
                req.Headers.Add(HttpRequestHeader.UserAgent, "GHKAKA");
                req.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + Token);
                req.Headers.Add(HttpRequestHeader.Accept, "*/*");
                req.Headers.Add(HttpRequestHeader.Connection, "keep-alive");
                return req.GetResponseAsync();
            } catch(Exception){
                throw;
            }
        }
        static void SetProperty(object any, string propertyName, object value) {
            PropertyInfo[] info = any.GetType().GetProperties();
            foreach(var item in info) {
                if(propertyName == item.Name) {
                    item.SetValue(any, value);
                    break;
                }
            }
        }
    }
}