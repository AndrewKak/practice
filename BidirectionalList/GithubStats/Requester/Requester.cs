﻿using System;
using System.Collections.Generic;
using System.Net.Http;
namespace GithubStats {
    public abstract class Requester {
        public string ProductName { get; private set; }
        public string OrgName { get; private set; }
        protected internal string Token { get; private set; }
        protected internal ICollection<Repository> Repositories { get; set; }
        private DataStorage DataStorage { get; set; }
        public Requester(string orgName, string productName, string token, DataStorage dataStorage) {
            Token = token;
            OrgName = orgName;
            ProductName = productName;
            DataStorage = dataStorage;
        }
        internal void DownloadInfo() {
            foreach(var repo in Repositories) {
                AddInfo<Views>(repo);
                AddInfo<Clones>(repo);
                DataStorage.UpdateTable(repo);
            }
        }
        protected internal abstract void SetRepos();
        protected internal abstract void AddInfo<T>(Repository repo);
    }
}