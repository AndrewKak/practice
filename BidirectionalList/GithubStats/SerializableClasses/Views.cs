﻿using System;
namespace GithubStats {
    public class Views {
        public int count { get; set; }
        public int uniques { get; set; }
        public ViewsPerDate[] views { get; set; }
        public ViewsPerDate FindByDate(DateTime dateTime) {
            foreach(ViewsPerDate viewsPerDate in views) {
                if(viewsPerDate.timestamp == dateTime) {
                    return viewsPerDate;
                }
            }
            return new ViewsPerDate(dateTime, 0, 0);
        }
    }
    public class ViewsPerDate {
        public DateTime timestamp { get; set; }
        public int count { get; set; }
        public int uniques { get; set; }
        public ViewsPerDate(DateTime timestamp, int count, int uniques) {
            this.timestamp = timestamp;
            this.count = count;
            this.uniques = uniques;
        }


    }
}