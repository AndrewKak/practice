﻿namespace GithubStats {
    public class Repository {
        public string name { get; set; }
        public Views views { get; set; }
        public Clones clones { get; set; }
        public ClonesPerDate[] GetClonesArray(){
            return clones.clones;
        }
        public ViewsPerDate[] GetViewsArray() {
            return views.views;
        }
    }
}