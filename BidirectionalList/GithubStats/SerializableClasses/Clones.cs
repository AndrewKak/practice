﻿using System;
namespace GithubStats {
    public class Clones {
        public int count { get; set; }
        public int uniques { get; set; }
        public ClonesPerDate[] clones { get; set; }
        public ClonesPerDate FindByDate(DateTime dateTime) {
            foreach(ClonesPerDate clonesPerDate in clones) {
                if(clonesPerDate.timestamp == dateTime) {
                    return clonesPerDate;
                }
            }
            return new ClonesPerDate(dateTime, 0, 0);
        }
    }
    public class ClonesPerDate {
        public DateTime timestamp { get; set; }
        public int count { get; set; }
        public int uniques { get; set; }
        public ClonesPerDate(DateTime timestamp, int count, int uniques) {
            this.timestamp = timestamp;
            this.count = count;
            this.uniques = uniques;
        }
    }
}