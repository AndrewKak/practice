﻿namespace GithubStats {
    public abstract class DataStorage {
        protected string ConnectionString { get; private set; }
        protected string DataBase { get; private set; }
        protected static string TableName { get; set; }
        public void UpdateTable(Repository repository) {
              CheckStorage();
              UpdateInfo(repository);            
        }
        protected DataStorage(string connectionString, string database) {
            ConnectionString = connectionString;
            DataBase = database;
        }
        protected internal abstract void CreateTable();
        protected internal abstract void UpdateInfo(Repository repository);
        protected internal abstract void CheckStorage();
    }
}