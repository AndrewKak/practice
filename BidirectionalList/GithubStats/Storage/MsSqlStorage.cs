﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
namespace GithubStats {
    class MsSqlStorage : DataStorage {
        string CreateString{ get; set; }
        string InsertString { get; set; }
        public MsSqlStorage(string connectionString, string database) : base(connectionString, database) {
            TableName = "GithubStats";
            CreateString = @$"Create TABLE {TableName} (Name varchar(50), Date DATE, UniquesViews INT,
                            Views INT, UniquesClones INT,Clones INT, constraint PK_Info PRIMARY KEY (Name ,Date));";
            InsertString = @$"Begin if not Exists ( Select * from {TableName} where Date = @CurrentDate and Name = @Name) 
                                        Begin Insert Into {TableName} ( Name, Date, UniquesViews ,Views, UniquesClones, Clones) 
                                        Values (  @Name, @CurrentDate,@UniquesViews, @Views, @UniquesClones, @Clones)
                                        End 
                                    else Update  {TableName} SET  UniquesViews = @UniquesViews,Views = @Views, 
                                    UniquesClones = @UniquesClones, Clones = @Clones Where Date = @CurrentDate and Name = @Name
                            End";
        }
        protected internal override void CreateTable() {
            using(SqlConnection connection = new SqlConnection(ConnectionString)) {
                SqlCommand command = new SqlCommand(CreateString, connection);
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
        protected internal override void UpdateInfo(Repository repository) {
            using(SqlConnection connection = new SqlConnection(ConnectionString)) {
                SqlCommand command = new SqlCommand(InsertString, connection);
                command.Parameters.AddWithValue("@Name", repository.name);
                ViewsPerDate[] viewsPerDates = repository.GetViewsArray();
                ClonesPerDate[] clonesPerDates = repository.GetClonesArray();
                InitCommandParameters(command);
                List<DateTime> dateTimes = new List<DateTime>();
                foreach(ViewsPerDate views in viewsPerDates) {
                    dateTimes.Add(views.timestamp);
                }
                foreach(ClonesPerDate clones in clonesPerDates) {
                    DateTime dateTime = clones.timestamp;
                    if(!dateTimes.Contains(dateTime)){
                        dateTimes.Add(dateTime);
                    }
                }
                dateTimes.Sort();
                connection.Open();
                foreach(DateTime date in dateTimes){
                    SetCommandForDate(command, repository.views.FindByDate(date), repository.clones.FindByDate(date));
                    command.ExecuteNonQuery();
                }
            }
        }
        protected internal override void CheckStorage() {
            try {
                using(SqlConnection connection = new SqlConnection(ConnectionString)) {
                    connection.Open();
                }
            } catch(Exception) {
                throw;
            }
            int existCode;
            using(SqlConnection connection = new SqlConnection(ConnectionString)) {
                    SqlCommand sqlCommand = new SqlCommand(@$"If EXISTS (SELECT * 
                    FROM {DataBase}.INFORMATION_SCHEMA.TABLES
                    WHERE TABLE_SCHEMA = 'dbo'
                    AND TABLE_NAME = '{TableName}')
                    SELECT 1 AS res ELSE SELECT 0 AS res;", connection);
                    connection.Open();
                    existCode =  (int)sqlCommand.ExecuteScalar();
            }
            if(existCode == 0) {
                CreateTable(); 
            }
        }
        void SetCommandForDate(SqlCommand command, ViewsPerDate viewsPerDate, ClonesPerDate clonesPerDate) {
            command.Parameters["@CurrentDate"].Value = viewsPerDate.timestamp.Date;
            command.Parameters["@UniquesViews"].Value = viewsPerDate.uniques;
            command.Parameters["@Views"].Value = viewsPerDate.count;
            command.Parameters["@UniquesClones"].Value = clonesPerDate.uniques;
            command.Parameters["@Clones"].Value = clonesPerDate.count;
        }
        void InitCommandParameters(SqlCommand command){
            command.Parameters.Add("@CurrentDate", SqlDbType.Date);
            command.Parameters.Add("@UniquesViews", SqlDbType.Int);
            command.Parameters.Add("@Views", SqlDbType.Int);
            command.Parameters.Add("@UniquesClones", SqlDbType.Int);
            command.Parameters.Add("@Clones", SqlDbType.Int);
        }
    }
}
