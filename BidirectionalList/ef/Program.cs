﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace ef {
    class Program {
        static void Main(string[] args) {
            Car car = new Car();
            car.name = "bbb";
            DataContext dataContext = new DataContext();
            dataContext.Cars.Add(car);
            dataContext.SaveChanges();
        }
    }
    
    public class Car  {
        [Key]
        public string name{ get; set; }
        public string model{ get; set; }
    }
    public partial class DataContext : DbContext {
        public DataContext() : base( ) {
        }
        public DbSet<Car> Cars { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseSqlServer(@"Data Source=STUDENT22-W10;Initial Catalog=CarsData;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }


    }

}

