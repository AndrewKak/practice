using NUnit.Framework;
using System;
using BidirectionalList;
using System.Linq;

namespace BidirectionalListTests {
    public class Int32Tests {
        BidirectionalList<int> emptyBidirectionalList;
        BidirectionalList<int> singleBidirectionalList;
        BidirectionalList<int> someBidirectionalList;
        int[] singleArray = { 1 };
        int[] someArray = { 1, 2, 3, 4, 5 };
        int[] emptyArray = new int[0];
        static void ListCheck<T>(T[] arr, Func<int,int> func,int length) {
            int i;
            for(i = 0; i < arr.Length; i++) {
                if(arr[i].ToString() != func(i).ToString()) {
                    throw new Exception();
                }
            }
            if(arr.Length != length) {
                throw new Exception();
            }
        }
        void TripleArrayCheck(int[] arrayTest1, int[] arrayTest2, int[] arrayTest3) {
            ListCheck(arrayTest1, emptyBidirectionalList.FindByIndex, emptyBidirectionalList.Length);
            ListCheck(arrayTest2,  singleBidirectionalList.FindByIndex, singleBidirectionalList.Length);
            ListCheck(arrayTest3, someBidirectionalList.FindByIndex, someBidirectionalList.Length);
        }
        [SetUp]
        public void SetupTest() {
            emptyBidirectionalList = new BidirectionalList<int>(singleArray);
            emptyBidirectionalList.DeleteByIndex(0);
            singleBidirectionalList = new BidirectionalList<int>(singleArray);
            someBidirectionalList = new BidirectionalList<int>(someArray);
        }
        [Test]
        public void ReturnTypeTest() {
            Assert.IsTrue(typeof(Int32) == emptyBidirectionalList.ReturnContentType());
            Assert.IsTrue(typeof(Int32) == singleBidirectionalList.ReturnContentType());
            Assert.IsTrue(typeof(Int32) == someBidirectionalList.ReturnContentType());
        }
        [Test]
        public void ListCreateTest() {
            Assert.DoesNotThrow(() => emptyBidirectionalList = new BidirectionalList<int>(null));
            ListCheck(emptyArray, emptyBidirectionalList.FindByIndex, emptyBidirectionalList.Length);
            Assert.DoesNotThrow(() => emptyBidirectionalList = new BidirectionalList<int>(new int[0]));
            ListCheck(emptyArray, emptyBidirectionalList.FindByIndex, emptyBidirectionalList.Length);
        }
        [Test]
        public void AdddFirstElementTest() {
            int[] arrayTest1 = { -5 };
            int[] arrayTest2 = { -5, 1 };
            int[] arrayTest3 = { -5, 1, 2, 3, 4, 5 };
            emptyBidirectionalList.AddFirst(-5);
            singleBidirectionalList.AddFirst(-5);
            someBidirectionalList.AddFirst(-5);
            Assert.IsTrue(-5 == emptyBidirectionalList.FindByIndex(0));
            Assert.IsTrue(-5 == singleBidirectionalList.FindByIndex(0));
            Assert.IsTrue(-5 == someBidirectionalList.FindByIndex(0));
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        [Test]
        public void AddAfterElementNotInListTest() {
            Assert.Throws<ElementNotFoundException>(() => emptyBidirectionalList.AddAfter(0, 6));
            Assert.Throws<ElementNotFoundException>(() => singleBidirectionalList.AddAfter(0, 6));
            Assert.Throws<ElementNotFoundException>(() => someBidirectionalList.AddAfter(0, 6));
        }
        [Test]
        public void AddAfterElementTest() {
            int[] arrayTest2 = { 1, 6 };
            int[] arrayTest3 = { 1, 6, 2, 3, 4, 5 };
            singleBidirectionalList.AddAfter(1, 6);
            someBidirectionalList.AddAfter(1, 6);
            Assert.IsTrue(6 == singleBidirectionalList.FindByIndex(1));
            Assert.IsTrue(6 == someBidirectionalList.FindByIndex(1));
            ListCheck(arrayTest2, singleBidirectionalList.FindByIndex, singleBidirectionalList.Length);
            ListCheck(arrayTest3, someBidirectionalList.FindByIndex, someBidirectionalList.Length);
        }
        [Test]
        public void AddAtPositionOutOfRangeTest() {
            Assert.Throws<IndexOutOfRangeException>(() => emptyBidirectionalList.AddAtPosition(-1, 6));
            Assert.Throws<IndexOutOfRangeException>(() => singleBidirectionalList.AddAtPosition(-1, 6));
            Assert.Throws<IndexOutOfRangeException>(() => someBidirectionalList.AddAtPosition(102112, 6));
        }
        [Test]
        public void AddAtPositionTest() {
            int[] arrayTest1 = { 6 };
            int[] arrayTest2 = { 1, 6 };
            int[] arrayTest3 = { 1, 6, 2, 3, 4, 5 };
            emptyBidirectionalList.AddAtPosition(0, 6);
            singleBidirectionalList.AddAtPosition(1, 6);
            someBidirectionalList.AddAtPosition(1, 6);
            Assert.IsTrue(6 == emptyBidirectionalList.FindByIndex(0));
            Assert.IsTrue(6 == singleBidirectionalList.FindByIndex(1));
            Assert.IsTrue(6 == someBidirectionalList.FindByIndex(1));
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        [Test]
        public void FindByIndexOutOfRangeTest() {
            Assert.Throws<IndexOutOfRangeException>(() => emptyBidirectionalList.FindByIndex(-1));
            Assert.Throws<IndexOutOfRangeException>(() => someBidirectionalList.FindByIndex(161516));
        }
        [Test]
        public void FindByIndexTest() {
            Assert.IsTrue(1 == singleBidirectionalList.FindByIndex(0));
            Assert.IsTrue(2 == someBidirectionalList.FindByIndex(1));
        }
        [Test]
        public void FindByValueNotInListTest() {
            Assert.Throws<ElementNotFoundException>(() => emptyBidirectionalList.FindIndexByValue(0));
            Assert.Throws<ElementNotFoundException>(() => singleBidirectionalList.FindIndexByValue(0));
        }
        [Test]
        public void FindByValueTest() {
            Assert.IsTrue(0 == singleBidirectionalList.FindIndexByValue(1));
            Assert.IsTrue(1 == someBidirectionalList.FindIndexByValue(2));
        }
        [Test]
        public void FindByConditionNullFuncTest() {
            Assert.Throws<ArgumentNullException>(() => emptyBidirectionalList.FindByCondition(null));
            Assert.Throws<ArgumentNullException>(() => singleBidirectionalList.FindByCondition(null));
            Assert.Throws<ArgumentNullException>(() => someBidirectionalList.FindByCondition(null));
        }
        [Test]
        public void FindByConditionTest() {
            Assert.Throws<ElementNotFoundException>(() => emptyBidirectionalList.FindByCondition(x => x == x + x));
            Assert.IsTrue(1 == singleBidirectionalList.FindByCondition(x => x == x * x));
            Assert.IsFalse(0 == someBidirectionalList.FindByCondition(x => x == x * x));
        }
        [Test]
        public void DeleteByIndexOutOfRangeTest() {
            Assert.Throws<IndexOutOfRangeException>(() => emptyBidirectionalList.DeleteByIndex(-1));
            Assert.Throws<IndexOutOfRangeException>(() => singleBidirectionalList.DeleteByIndex(100));
            Assert.Throws<IndexOutOfRangeException>(() => someBidirectionalList.DeleteByIndex(105));
        }
        [Test]
        public void DeleteByIndexTest() {
            int[] arrayTest1 = Array.Empty<int>();
            int[] arrayTest2 = Array.Empty<int>();
            int[] arrayTest3 = { 2, 3, 4 };
            Assert.DoesNotThrow(() => singleBidirectionalList.DeleteByIndex(0));
            Assert.DoesNotThrow(() => someBidirectionalList.DeleteByIndex(0));
            Assert.DoesNotThrow(() => someBidirectionalList.DeleteByIndex(someBidirectionalList.Length - 1));
            Assert.IsTrue(singleBidirectionalList.Length == 0);
            Assert.IsTrue(someBidirectionalList.Length == 3);
            Assert.IsTrue(someBidirectionalList.FindByIndex(0) == 2);
            Assert.IsTrue(someBidirectionalList.FindByIndex(2) == 4);
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        [Test]
        public void DeleteByValueNotInListTest() {
            Assert.DoesNotThrow(() => emptyBidirectionalList.DeleteByValue(-1));
            Assert.DoesNotThrow(() => singleBidirectionalList.DeleteByValue(100));
            Assert.DoesNotThrow(() => someBidirectionalList.DeleteByValue(105));
        }
        [Test]
        public void DeleteByValueTest() {
            int[] arrayTest1 = Array.Empty<int>();
            int[] arrayTest2 = Array.Empty<int>();
            int[] arrayTest3 = { 2, 3, 4 };
            emptyBidirectionalList.DeleteByValue(1);
            singleBidirectionalList.DeleteByValue(1);
            someBidirectionalList.DeleteByValue(1);
            someBidirectionalList.DeleteByValue(5);
            Assert.IsTrue(singleBidirectionalList.Length == 0);
            Assert.IsTrue(someBidirectionalList.Length == 3);
            Assert.IsTrue(someBidirectionalList.FindByIndex(0) == 2);
            Assert.IsTrue(someBidirectionalList.FindByIndex(2) == 4);
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        [Test]
        public void DeleteByConditionNullFunc() {
            Assert.Throws<ArgumentNullException>(() => emptyBidirectionalList.DeleteByCondition(null));
            Assert.Throws<ArgumentNullException>(() => singleBidirectionalList.DeleteByCondition(null));
            Assert.Throws<ArgumentNullException>(() => someBidirectionalList.DeleteByCondition(null));
        }
        [Test]
        public void DeleteByConditionTest() {
            int[] arrayTest1 = Array.Empty<int>();
            int[] arrayTest2 = Array.Empty<int>();
            int[] arrayTest3 = { 2, 3, 4 };
            Assert.DoesNotThrow(() => emptyBidirectionalList.DeleteByCondition(x => x == x + x));
            Assert.DoesNotThrow(() => singleBidirectionalList.DeleteByCondition(x => x == x + x));
            Assert.DoesNotThrow(() => someBidirectionalList.DeleteByCondition(x => x == x + x));
            Assert.DoesNotThrow(() => singleBidirectionalList.DeleteByCondition(x => x == x * x));
            Assert.DoesNotThrow(() => someBidirectionalList.DeleteByCondition(x => x == x * x));
            Assert.DoesNotThrow(() => someBidirectionalList.DeleteByCondition(x => x == 5));
            Assert.IsTrue(singleBidirectionalList.Length == 0);
            Assert.IsTrue(someBidirectionalList.Length == 3);
            Assert.IsTrue(someBidirectionalList.FindByIndex(0) == 2);
            Assert.IsTrue(someBidirectionalList.FindByIndex(2) == 4);
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        [Test]
        public void Test666(){ 
            BidirectionalList<int> list= new BidirectionalList<int>(someArray);
            list.Clear();
            list.Add(5);
        }
    }
}