﻿using NUnit.Framework;
using System;
using BidirectionalList;
using CarEngineReference;
namespace BidirectionalListTests {
    public class TestsForCar {
        BidirectionalList<Car> emptyCarList;
        BidirectionalList<Car> singleCarList;
        BidirectionalList<Car> someCarList;
        Car[] emptyCarArray = new Car[0];
        static Car carR8 = new Car("R8", 1, 2, 3), carR0 = new Car("R0", 1, 2, 3), carR1 = new Car("R1", 1, 2, 3), carR2 = new Car("R2", 1, 2, 3), carR3 = new Car("R3", 1, 2, 3), carR4 = new Car("R4", 1, 2, 3);
        Car[] singleCarArray = { carR0 };
        Car[] someCarArray = { carR0, carR1, carR2, carR3, carR4 };
        Car testCar, sameCarR0 = new Car("R0", 1, 2, 3);
        Car[] arrayTest1, arrayTest2, arrayTest3;
        static void ListCheck(Car[] arr, Func<int, Car> func, int length) {
            int i;
            for(i = 0; i < arr.Length; i++) {
                if(arr[i].Name != func(i).Name) {
                    throw new Exception();
                }
            }
            if(arr.Length != length) {
                throw new Exception();
            }
        }
        void TripleArrayCheck(Car[] arrayTest1, Car[] arrayTest2, Car[] arrayTest3) {
            ListCheck(arrayTest1, emptyCarList.FindByIndex, emptyCarList.Length);
            ListCheck(arrayTest2, singleCarList.FindByIndex, singleCarList.Length);
            ListCheck(arrayTest3, someCarList.FindByIndex, someCarList.Length);
        }
        [SetUp]
        public void SetupTest() {
            emptyCarList = new BidirectionalList<Car>(singleCarArray);
            emptyCarList.DeleteByIndex(0);
            singleCarList = new BidirectionalList<Car>(singleCarArray);
            someCarList = new BidirectionalList<Car>(someCarArray);
            testCar = new Car("X5", 123213, 555, 45);
            arrayTest1 = new Car[] { testCar };
            arrayTest2 = new Car[] { carR0, testCar };
            arrayTest3 = new Car[] { carR0, testCar, carR1, carR2, carR3, carR4 };
        }
        [Test]
        public void ReturnTypeTest() {
            Assert.IsTrue(typeof(Car) == emptyCarList.ReturnContentType());
            Assert.IsTrue(typeof(Car) == singleCarList.ReturnContentType());
            Assert.IsTrue(typeof(Car) == someCarList.ReturnContentType());
        }
        [Test]
        public void ListCreateTest() {
            Assert.DoesNotThrow(() => emptyCarList = new BidirectionalList<Car>(null));
            ListCheck(emptyCarArray, emptyCarList.FindByIndex, emptyCarList.Length);
            Assert.DoesNotThrow(() => emptyCarList = new BidirectionalList<Car>(new Car[0]));
            ListCheck(emptyCarArray, emptyCarList.FindByIndex, emptyCarList.Length);
        }
        [Test]
        public void AdddFirstElementTest() {
            arrayTest1 = new Car[] { testCar };
            arrayTest2 = new Car[] { testCar, carR0 };
            arrayTest3 = new Car[] { testCar, carR0, carR1, carR2, carR3, carR4 };
            emptyCarList.AddFirst(testCar);
            singleCarList.AddFirst(testCar);
            someCarList.AddFirst(testCar);
            Assert.IsTrue(testCar == emptyCarList.FindByIndex(0));
            Assert.IsTrue(testCar == singleCarList.FindByIndex(0));
            Assert.IsTrue(testCar == someCarList.FindByIndex(0));
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        [Test]
        public void AddAfterElementNotInListTest() {
            Assert.Throws<ElementNotFoundException>(() => emptyCarList.AddAfter(carR0, testCar));
            Assert.Throws<ElementNotFoundException>(() => singleCarList.AddAfter(carR8, testCar));
            Assert.Throws<ElementNotFoundException>(() => someCarList.AddAfter(carR8, testCar));
        }
        [Test]
        public void AddAfterElementTest() {
            singleCarList.AddAfter(carR0, testCar);
            Assert.IsTrue(testCar == singleCarList.FindByIndex(1));
            ListCheck(arrayTest2, singleCarList.FindByIndex, singleCarList.Length);
        }
        [Test]
        public void AddAfterElement2Test() {
            singleCarList.AddAfter(sameCarR0, testCar);
            Assert.IsTrue(testCar == singleCarList.FindByIndex(1));
            arrayTest2[0] = sameCarR0;
            ListCheck(arrayTest2, singleCarList.FindByIndex, singleCarList.Length);
        }
        [Test]
        public void AddAtPositionOutOfRangeTest() {
            Assert.Throws<IndexOutOfRangeException>(() => emptyCarList.AddAtPosition(-1, testCar));
            Assert.Throws<IndexOutOfRangeException>(() => singleCarList.AddAtPosition(-1, testCar));
            Assert.Throws<IndexOutOfRangeException>(() => someCarList.AddAtPosition(102112, testCar));
        }
        [Test]
        public void AddAtPositionTest() {
            emptyCarList.AddAtPosition(0, testCar);
            singleCarList.AddAtPosition(1, testCar);
            someCarList.AddAtPosition(1, testCar);
            Assert.IsTrue("X5" == emptyCarList.FindByIndex(0).Name);
            Assert.IsTrue("X5" == singleCarList.FindByIndex(1).Name);
            Assert.IsTrue("X5" == someCarList.FindByIndex(1).Name);
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        [Test]
        public void FindByIndexOutOfRangeTest() {
            Assert.Throws<IndexOutOfRangeException>(() => emptyCarList.FindByIndex(-1));
            Assert.Throws<IndexOutOfRangeException>(() => someCarList.FindByIndex(161516));
        }
        [Test]
        public void FindByIndexTest() {
            Assert.IsTrue("R0" == singleCarList.FindByIndex(0).Name);
            Assert.IsTrue("R1" == someCarList.FindByIndex(1).Name);
        }
        [Test]
        public void FindIndexByValueNotInListTest() {
            Assert.Throws<ElementNotFoundException>(() => emptyCarList.FindIndexByValue(new Car("R8", 1, 2, 3)));
            Assert.Throws<ElementNotFoundException>(() => singleCarList.FindIndexByValue(new Car("R7", 1, 2, 3)));
        }
        [Test]
        public void FindByValueTest() {
            Assert.IsTrue(0 == singleCarList.FindIndexByValue(singleCarList.FindByIndex(0)));
            Assert.IsTrue(2 == someCarList.FindIndexByValue(someCarList.FindByIndex(2)));
        }
        [Test]
        public void FindByValue2Test() {
            Assert.IsTrue(sameCarR0.Equals(someCarList.FindByValue(sameCarR0)));
            Assert.IsFalse(carR8.Equals(someCarList.FindByValue(sameCarR0)));
        }
        [Test]
        public void FindByConditionNullFuncTest() {
            Assert.Throws<ArgumentNullException>(() => emptyCarList.FindByCondition(null));
            Assert.Throws<ArgumentNullException>(() => singleCarList.FindByCondition(null));
            Assert.Throws<ArgumentNullException>(() => someCarList.FindByCondition(null));
        }
        [Test]
        public void FindByConditionTest() {
            Assert.Throws<ElementNotFoundException>(() => emptyCarList.FindByCondition(x => x.Name == "R1"));
            Assert.Throws<ElementNotFoundException>(() => singleCarList.FindByCondition(x => x.Name == "R8"));
            Assert.IsTrue("R1" == someCarList.FindByCondition(x => x.Name == "R1").Name);
        }
        [Test]
        public void DeleteByIndexOutOfRangeTest() {
            Assert.Throws<IndexOutOfRangeException>(() => emptyCarList.DeleteByIndex(-1));
            Assert.Throws<IndexOutOfRangeException>(() => singleCarList.DeleteByIndex(100));
            Assert.Throws<IndexOutOfRangeException>(() => someCarList.DeleteByIndex(105));
        }
        [Test]
        public void DeleteByIndexTest() {
            DeleteArrayChange();
            Assert.DoesNotThrow(() => emptyCarList.DeleteByIndex(0));
            Assert.DoesNotThrow(() => singleCarList.DeleteByIndex(0));
            Assert.DoesNotThrow(() => someCarList.DeleteByIndex(0));
            Assert.DoesNotThrow(() => someCarList.DeleteByIndex(someCarList.Length - 1));
            Assert.IsTrue(singleCarList.Length == 0);
            Assert.IsTrue(someCarList.Length == 3);
            Assert.IsTrue(someCarList.FindByIndex(0).Name == "R1");
            Assert.IsTrue(someCarList.FindByIndex(2).Name == "R3");
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        [Test]
        public void DeleteByValueNotInListTest() {
            Assert.DoesNotThrow(() => emptyCarList.DeleteByValue(carR0));
            Assert.DoesNotThrow(() => singleCarList.DeleteByValue(carR0));
            Assert.DoesNotThrow(() => someCarList.DeleteByValue(carR8));
        }
        [Test]
        public void DeleteByValue2Test() {
            BidirectionalList<Car> list = new BidirectionalList<Car>();
            list.Add(new Car("a", 1, 2, 3));
            list.Add(new Car("b", 4, 5, 6));
            list.Add(new Car("a", 1, 2, 3));
            list.DeleteByValue(new Car("a", 1, 2, 3));
            Assert.IsTrue(list.FindByIndex(0).Name == "b");
            Assert.IsTrue(list.Length == 1);
        }
        [Test]
        public void DeleteByValueTest() {
            DeleteArrayChange();
            emptyCarList.DeleteByValue(carR1);
            singleCarList.DeleteByValue(carR0);
            someCarList.DeleteByValue(carR0);
            someCarList.DeleteByValue(carR4);
            Assert.IsTrue(singleCarList.Length == 0);
            Assert.IsTrue(someCarList.Length == 3);
            Assert.IsTrue(someCarList.FindByIndex(0).Name == "R1");
            Assert.IsTrue(someCarList.FindByIndex(2).Name == "R3");
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        [Test]
        public void DeleteByConditionNullFunc() {
            Assert.Throws<ArgumentNullException>(() => emptyCarList.DeleteByCondition(null));
            Assert.Throws<ArgumentNullException>(() => singleCarList.DeleteByCondition(null));
            Assert.Throws<ArgumentNullException>(() => someCarList.DeleteByCondition(null));
        }
        [Test]
        public void DeleteByConditionTest() {
            DeleteArrayChange();
            Assert.DoesNotThrow(() => emptyCarList.DeleteByCondition(x => x.Name == "R1"));
            Assert.DoesNotThrow(() => singleCarList.DeleteByCondition(x => x.Name == "R8"));
            Assert.DoesNotThrow(() => someCarList.DeleteByCondition(x => x.Name == "R8"));
            Assert.DoesNotThrow(() => singleCarList.DeleteByCondition(x => x.Name == "R0"));
            Assert.DoesNotThrow(() => someCarList.DeleteByCondition(x => x.Name == "R4"));
            Assert.DoesNotThrow(() => someCarList.DeleteByCondition(x => x.Name == "R0"));
            Assert.IsTrue(singleCarList.Length == 0);
            Assert.IsTrue(someCarList.Length == 3);
            Assert.IsTrue(someCarList.FindByIndex(0).Name == "R1");
            Assert.IsTrue(someCarList.FindByIndex(2).Name == "R3");
            TripleArrayCheck(arrayTest1, arrayTest2, arrayTest3);
        }
        void DeleteArrayChange() {
            arrayTest1 = Array.Empty<Car>();
            arrayTest2 = Array.Empty<Car>();
            arrayTest3 = new Car[] { carR1, carR2, carR3 };
        }
    }
}