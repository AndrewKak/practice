﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BidirectionalList;
using CarEngineReference;
namespace PerformanceMeasuring {

    public class TimeMeasurement  {
        const int length = 5000;
        List<TimeResult> allResults;
        BidirectionalList<double> bidirectionalListValue;
        BidirectionalList<Car> bidirectionalListReference;
        List<double> listValue;
        List<Car> listReference;
        ArrayList arrayListValue;
        ArrayList arrayListReference;
        double[] valueArray = new double[length];
        Car[] referenceArray = new Car[length];
        double[] array = new double[length];
        Car[] carArray = new Car[length];
        delegate TResult someDelegate<T, TResult>(T item);
        private struct TimeResult {
            public Type CollectionType { get; private set; }
            public TimeSpan Time { get; private set; }
            public string OperationName { get; private set; }
            public TimeResult(string operationName, Type collectionType, TimeSpan time) {
                CollectionType = collectionType;
                OperationName = operationName;
                Time = time;
            }
        }
        public TimeMeasurement() {
            allResults = new List<TimeResult>();
            valueArray = new double[length];
            carArray = new Car[length];
            bidirectionalListReference = new BidirectionalList<Car>();
            listReference = new List<Car>();
            arrayListReference = new ArrayList();
            for(int i = 0; i < length; i++) {
                array[i] = i;
                valueArray[i] = i;
                carArray[i] = new Car(" ", i, 0, 0);
                referenceArray[i] = new Car(" ", i, 0, 0);
                bidirectionalListReference.Add(new Car(" ", i, 0, 0));
                listReference.Add(new Car(" ", i, 0, 0));
                arrayListReference.Add(new Car(" ", i, 0, 0));
            }
            bidirectionalListValue = new BidirectionalList<double>(array);
            listValue = new List<double>(array);
            arrayListValue = new ArrayList(array);
        }
        public void Measure() {
            double item = 5; object addObject = 5;
            Car carFind = new Car(" ", 4999, 0, 0);
            Car carAdd = new Car(" ", 10000, 0, 0);
            double searchValue = 4999;
            Predicate<double> predicateValue = (predicate) => predicate == 4999;
            Predicate<Car> predicateReference = (predicate) => predicate == carFind;
            object searchObject = 4999; 
            MeasureMethod(bidirectionalListValue.GetType(), "AddDouble", bidirectionalListValue.Add, item);
            MeasureMethod(listValue.GetType(), "AddDouble", listValue.Add, item);
            MeasureMethod(arrayListValue.GetType(), "AddDouble", arrayListValue.Add, addObject);

            MeasureMethod(bidirectionalListValue.GetType(), "FindDouble", bidirectionalListValue.FindIndexByValue, searchValue);
            MeasureMethod(listValue.GetType(), "FindDouble", listValue.Find, predicateValue);
            MeasureMethod(arrayListValue.GetType(), "FindDouble", arrayListValue.IndexOf, searchObject);
            MeasureMethod(valueArray.GetType(), "FindDouble", Array.Find, valueArray, predicateValue);

            MeasureMethod(bidirectionalListValue.GetType(), "DeleteDouble", bidirectionalListValue.DeleteByValue, searchValue);
            MeasureMethod(listValue.GetType(), "DeleteDouble", listValue.Remove, searchValue);
            MeasureMethod(arrayListValue.GetType(), "DeleteDouble", x => arrayListValue.Remove(x), searchValue);
            ////////////////////
            MeasureMethod(bidirectionalListReference.GetType(), "AddCar", bidirectionalListReference.Add, carAdd);
            MeasureMethod(listReference.GetType(), "AddCar", listReference.Add, carAdd);
            MeasureMethod(arrayListReference.GetType(), "AddCar", arrayListReference.Add, carAdd);

            MeasureMethod(bidirectionalListReference.GetType(), "FindCar", bidirectionalListReference.FindIndexByValue, carFind);
            MeasureMethod(listReference.GetType(), "FindCar", listReference.Find, predicateReference);
            MeasureMethod(arrayListReference.GetType(), "FindCar", arrayListReference.IndexOf, carFind);
            MeasureMethod(referenceArray.GetType(), "FindCar", Array.Find, referenceArray, predicateReference);

            MeasureMethod(bidirectionalListReference.GetType(), "DeleteCar", bidirectionalListReference.DeleteByValue, carFind);
            MeasureMethod(listReference.GetType(), "DeleteCar", listReference.Remove, carFind);
            MeasureMethod(arrayListReference.GetType(), "DeleteCar", x => arrayListReference.Remove(x), carFind);
            allResults= ResultSort(allResults);
            ResultOutput(allResults);
        }
        static void ResultOutput(List<TimeResult> results) {
            string previousName = String.Empty;
            foreach(TimeResult result in results) {
                string currentName = result.OperationName;
                if(currentName != previousName) {
                    Console.WriteLine();
                }
                Console.Write(result.CollectionType + " : " + result.CollectionType.Name + " : " + result.OperationName + " :  " + result.Time.TotalMilliseconds + "\n");
                previousName = currentName;
            }
            Console.WriteLine();
        }
        List<TimeResult> ResultSort(List<TimeResult> results) {
            List<TimeResult> sortResults = new List<TimeResult>();
            return sortResults = results.OrderBy(x => x.OperationName).ThenBy(x => x.Time).ToList();
        }
        void MeasureMethod<T>(Type collection, string operation, Action<T> action, T item) {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for(int j = 0; j < length; j++) {
                action(item);
            }
            stopwatch.Stop();
            allResults.Add(new TimeResult(operation, collection, stopwatch.Elapsed));
        }
        void MeasureMethod<T, TResult>(Type collection, string operation, Func<T, TResult> func, T item) {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for(int j = 0; j < length; j++) {
                func(item);
            }
            stopwatch.Stop();
            allResults.Add(new TimeResult(operation, collection, stopwatch.Elapsed));
        }
        void MeasureMethod<T1, T2, TResult>(Type collection, string operation, Func<T1, T2, TResult> func, T1 array, T2 item) {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for(int j = 0; j < length; j++) {
                func(array, item);
            }
            stopwatch.Stop();
            allResults.Add(new TimeResult(operation, collection, stopwatch.Elapsed));
        }
    }
}