using System;
using BidirectionalList;
using CarEngineReference;

namespace TestCar {
    class Test {
        static Func<int, bool> isSelfSquare = sqr => sqr == sqr * sqr;
        static void Main() {  
            int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Car carFirst = new Car(" ", 1, 2, 3);
            Car carSecond = new Car(" ", 1, 2, 3);
            bool x = carFirst.GetHashCode() == carSecond.GetHashCode();
            BidirectionalList<int> bidirectionalList = new BidirectionalList<int>(array);
            Output(bidirectionalList);
            bidirectionalList.AddFirst(0);
            Output(bidirectionalList);
            bidirectionalList.Add(10);
            Output(bidirectionalList);
            bidirectionalList.AddAfter(6, 7);
            Output(bidirectionalList);
            bidirectionalList.AddAtPosition(0, -1);
            Output(bidirectionalList);
            Console.WriteLine(bidirectionalList.FindByCondition(isSelfSquare));
            Console.WriteLine(bidirectionalList.FindByIndex(4));
            Console.WriteLine(bidirectionalList.FindIndexByValue(10));
            bidirectionalList.DeleteByCondition(isSelfSquare);
            Output(bidirectionalList);
            bidirectionalList.DeleteByIndex(10);
            Output(bidirectionalList);
            bidirectionalList.DeleteByValue(5);
            Output(bidirectionalList);
        }
        static void Output(BidirectionalList<int> bidirectionalList) {
            for(int i = 0; i < bidirectionalList.Length; i++) {
                Console.Write(bidirectionalList.FindByIndex(i) + "  ");
            }
            Console.WriteLine();
        }
    }
}