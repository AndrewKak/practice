﻿using BidirectionalList;
using CarSqlData;
using DatabaseLocal;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.IO;
using UserItem;
namespace SqlTests {
    public class SqlDataTests {
        const int LengthCars = 327;
        const int LengthUsers = 91;
        string wrongLocation = "|||users.json";
        string storageLocation = AppDomain.CurrentDomain.BaseDirectory + "users.xml";
        string storageLocation2 = @"D:\users.xml";
        string carsLocation = AppDomain.CurrentDomain.BaseDirectory + "cars.json";
        string usersLocation = AppDomain.CurrentDomain.BaseDirectory + "users.json";


        string webReference = "https://northwind.netcore.io/query/customers.json";
        static IConfigurationRoot GetConfig() {
            string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.Parent.FullName;
            IConfigurationBuilder builder = new ConfigurationBuilder().AddJsonFile(projectDirectory + @"\appsettings.json");
            return builder.Build();
        }
        string connection = GetConfig().GetConnectionString("myConnectionString");
        string localConnection = GetConfig().GetConnectionString("myLocalConnection");
        SqlXmlControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> baseList;
        SqlXmlControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> baseList2;
        public SqlXmlControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> CreateSqlMethod(string storageLocaition, string webReference) {
            return new SqlXmlControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>>(storageLocaition, webReference);
        }
        [SetUp]
        public void Setup() {
            File.Delete(storageLocation);
            File.Delete(storageLocation2);
            baseList2 = CreateSqlMethod(storageLocation, connection);
            baseList = CreateSqlMethod(storageLocation2, connection);
        }
        [Test]
        public void DeserializeTest() {
            SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> jsonCars = new SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>>(carsLocation, connection);
            File.Delete(storageLocation);
            File.Delete(storageLocation2);
            baseList2 = CreateSqlMethod(storageLocation, connection);
            baseList = CreateSqlMethod(storageLocation2, connection);
            CheckCollection(baseList.collection.Results, jsonCars.collection.Results);
            Assert.IsTrue(baseList.collection.Results.Count == LengthCars);
        }
        [Test]
        public void CheckAddressTest() {
            //Assert.Throws<ArgumentException>(() => CreateSqlMethod(storageLocation, string.Empty));
        }
        [Test]
        public void JsonSqlTest() {
            JsonSqlControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> jsonSql = new JsonSqlControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>>(localConnection, webReference);
            jsonSql.SaveCollection();
            //jsonSql.ReadDatabaseLocal();
            Assert.IsTrue(jsonSql.collection.Results.Count == LengthUsers);
        }
        [Test]
        public void AnyJsonSqlCarsTest() {
            SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> jsonCars = new SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>>(carsLocation, connection);
            AnyTypeSqlControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> anyCars = new AnyTypeSqlControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>>(localConnection, connection);
            anyCars.DownloadData();
            anyCars.SaveCollection();
            CheckCollection(jsonCars.collection.Results, anyCars.collection.Results);
            jsonCars.ReadDatabaseLocal();
            CheckCollection(jsonCars.collection.Results, anyCars.collection.Results);
            anyCars.DownloadData();
            CheckCollection(jsonCars.collection.Results, anyCars.collection.Results);
            anyCars.SaveCollection();
            CheckCollection(jsonCars.collection.Results, anyCars.collection.Results);
            anyCars.ReadDatabaseLocal();
        }
        [Test]
        public void AnyJsonSqlUsersTest() {
            JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>> jsonUsers = new JsonJsonControl<BidirectionalList<User>, User, Result<BidirectionalList<User>>>(usersLocation, webReference);
            //JsonSqlControl<BidirectionalList<User>, User, BidirectionalList<User>> jsonUsers = new JsonSqlControl<BidirectionalList<User>, User, BidirectionalList<User>>(connection , webReference);
            AnyTypeSqlControl<BidirectionalList<User>, User, BidirectionalList<User>> anyUsers = new AnyTypeSqlControl<BidirectionalList<User>, User, BidirectionalList<User>>(localConnection, connection);
            CheckCollection(jsonUsers.collection.Results, anyUsers.collection.Results);
            anyUsers.ReadDatabaseLocal();
            CheckCollection(jsonUsers.collection.Results, anyUsers.collection.Results);
            anyUsers.DownloadData();
            CheckCollection(jsonUsers.collection.Results, anyUsers.collection.Results);
            anyUsers.SaveCollection();
            CheckCollection(jsonUsers.collection.Results, anyUsers.collection.Results);
        }
        public void CheckCollection<T>(BidirectionalList<T> biList1, BidirectionalList<T> biList2) where T : IEquatable<T> {
            Assert.IsTrue(biList1.Count == biList2.Count);
            var enumerator1 = biList1.GetEnumerator();
            var enumerator2 = biList2.GetEnumerator();
            for(int i = 0; i < biList1.Length; i++) {
                Console.WriteLine(i);
                Assert.IsTrue(enumerator1.MoveNext().Equals(enumerator2.MoveNext()));
            }
        }
    }
}