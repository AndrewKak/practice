using BidirectionalList;
using CarSqlData;
using DatabaseLocal;
using NUnit.Framework;
using System;
using System.IO;
using System.Configuration;
using Microsoft.Extensions.Configuration;
namespace SqlTests {
    public class SqlJsonTests {
        const int Length = 327;
        string wrongLocation = "|||users.json";
        string storageLocation = AppDomain.CurrentDomain.BaseDirectory + "carsssss.json";
        string storageLocation2 = @"D:\users.json";
        string carsLocation = AppDomain.CurrentDomain.BaseDirectory + "cars.json";

        static IConfigurationRoot GetConfig() {
            string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.Parent.FullName;
            IConfigurationBuilder builder = new ConfigurationBuilder().AddJsonFile(projectDirectory + @"\appsettings.json");
            return builder.Build();
        }
        string connection = GetConfig().GetConnectionString("myConnectionString");
        string myConnectionString;// "Server=STUDENT22-W10;Database=CarsData;Trusted_Connection=True;";
        SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> baseList;
        SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> baseList2;
        public SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> CreateSqlMethod(string storageLocaition, string webReference) {
            return new SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>>(storageLocaition, webReference);
        }
        [SetUp]
        public void Setup() {
            myConnectionString = connection;
            File.Delete(storageLocation);
            File.Delete(storageLocation2);
            baseList2 = CreateSqlMethod(storageLocation, myConnectionString);
            baseList = CreateSqlMethod(storageLocation2, myConnectionString);
        }
        [Test]
        public void DeserializeTest() {
            SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>> jsonCars = new SqlJsonControl<BidirectionalList<CarInfo>, CarInfo, BidirectionalList<CarInfo>>(carsLocation, connection);
            File.Delete(storageLocation);
            File.Delete(storageLocation2);
            baseList2 = CreateSqlMethod(storageLocation, myConnectionString);
            baseList = CreateSqlMethod(storageLocation2, myConnectionString);
            Assert.IsTrue(baseList.collection.Results.Count == Length);
            CheckCollection(baseList.collection.Results, jsonCars.collection.Results);
            CheckCollection(baseList2.collection.Results, jsonCars.collection.Results);
        }
        [Test]
        public void CheckAddressTest() {
            Assert.Throws<InvalidOperationException>(() => CreateSqlMethod(storageLocation, string.Empty));
        }
        public void CheckCollection<T>(BidirectionalList<T> biList1, BidirectionalList<T> biList2) where T : IEquatable<T> {
            Assert.IsTrue(biList1.Count == biList2.Count);
            var enumerator1 = biList1.GetEnumerator();
            var enumerator2 = biList2.GetEnumerator();
            for(int i = 0; i < biList1.Length; i++) {
                Console.WriteLine(i);
                Assert.IsTrue(enumerator1.MoveNext().Equals(enumerator2.MoveNext()));
            }
        }
    }
}