﻿using System;

namespace TestCar {
    public class Car : IEquatable<Car> {
        public Engine Engine { get; private set; }
        public string Name { get; private set; }
        public int Cost { get; private set; }
        public Car(string name, int cost, float power, float consumption) {
            Engine = new Engine(power, consumption);
            Name = name;
            Cost = cost;
        }
        public bool Equals(Car other) => Engine.Equals(other.Engine) && Name == other.Name && Cost == other.Cost;
        public override int GetHashCode() {
            int hash = 2;
            hash = (hash * 7) + (!Object.ReferenceEquals(null, Engine) ? Engine.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.ReferenceEquals(null, Name) ? Name.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Cost) ? Cost.GetHashCode() : 0);
            return hash;
        }
    }
    public class Engine : IEquatable<Engine> {
        public float Power { get; private set; }
        public float Consumption { get; private set; }
        public Engine(float power, float consumption) {
            Power = power;
            Consumption = consumption;
        }
         public bool Equals(Engine other) => Power == other.Power && Consumption == other.Consumption;
        public override int GetHashCode() {
            int hash = 2;
            hash = (hash * 7) + (!Object.Equals(null, Power) ? Power.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Consumption) ? Consumption.GetHashCode() : 0);
            return hash;
        }
    }
}
