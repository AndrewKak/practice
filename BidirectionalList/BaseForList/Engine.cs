﻿using System;
namespace CarEngineReference {
    public class Engine : IEquatable<Engine> {
        public float Power { get; private set; }
        public float Consumption { get; private set; }
        public Engine(float power, float consumption) {
            Power = power;
            Consumption = consumption;
        }
        public bool Equals(Engine other) => Power == other.Power && Consumption == other.Consumption;
        public override int GetHashCode() {
            int hash = 2;
            hash = (hash * 7) + (!Object.Equals(null, Power) ? Power.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Consumption) ? Consumption.GetHashCode() : 0);
            return hash;
        }
    }
}