﻿using System;
namespace CarEngineReference {
    public class Car : IEquatable<Car> {
        public Engine Engine { get; private set; }
        public string Name { get; private set; }
        public int Cost { get; private set; }
        public Car(string name, int cost, float power, float consumption) {
            Engine = new Engine(power, consumption);
            Name = name;
            Cost = cost;
        }
        public bool Equals(Car other) => Engine.Equals(other.Engine) && Name == other.Name && Cost == other.Cost;
        public override int GetHashCode() {
            int hash = 2;
            hash = (hash * 7) + (!Object.ReferenceEquals(null, Engine) ? Engine.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.ReferenceEquals(null, Name) ? Name.GetHashCode() : 0);
            hash = (hash * 7) + (!Object.Equals(null, Cost) ? Cost.GetHashCode() : 0);
            return hash;
        }
    }
}