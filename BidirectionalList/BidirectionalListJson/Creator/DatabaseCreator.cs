﻿using BidirectionalList;
using System.Collections.Generic;
using System.IO;

namespace DatabaseLocal {
    internal class DatabaseCreator<TCollection, TItem, TResult> where TCollection : ICollection<TItem>, new() where TResult : IContainCollection<TCollection> {
        internal static void DatabaseCreate(AbstractDataControl<TCollection, TItem, TResult> collectionDatabase) {
            if(collectionDatabase.localData.CheckStorage()) {
                collectionDatabase.ReadDatabaseLocal();
            } else {
                collectionDatabase.DownloadData();
            }
        }
    }
}