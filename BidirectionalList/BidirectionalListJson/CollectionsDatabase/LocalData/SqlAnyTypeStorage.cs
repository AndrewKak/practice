﻿using CarSqlData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

namespace DatabaseLocal {
    internal class SqlAnyTypeStorage<TCollection, TItem> : LocalDataStorage<TCollection> where TCollection : ICollection<TItem>, new() where TItem : new() {
        public SqlAnyTypeStorage(string location) : base(location) {
        }
        DataTable Execute(string commandText) {
            DataTable dataTable = new DataTable();
            using(SqlConnection connection = new SqlConnection(StorageLocation)) {
                try {
                    using(SqlCommand command = new SqlCommand()) {
                        command.Connection = connection;
                        command.CommandType = CommandType.Text;
                        command.CommandText = commandText;
                        connection.Open();
                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        dataAdapter.Fill(dataTable);
                        return dataTable;
                    }
                } catch(SqlException) {
                    return dataTable;
                }
            }
        }
        static string GetTableName() {
            TItem item = new TItem();
            //return item.GetType().Name + "s";
            return "CarInfos";
        }
        TCollection DataToCollection(string commandText) {
            TCollection collection = new TCollection();
            var dataTable = Execute(commandText);
            foreach(DataRow row in dataTable.Rows) {
                 TItem item = XmlToItem(row);
                 collection.Add(item);
            }
            return collection;
        }
        string RowToXml(object data) {
            XmlSerializer ser = new XmlSerializer(typeof(TItem));
            using(var stringWriter = new StringWriter()) {
                ser.Serialize(stringWriter, data);
                return stringWriter.ToString().Replace("\'", "\'\'");
            }
        }
        TItem XmlToItem(DataRow row) {
            TItem item = new TItem();
            XmlSerializer ser = new XmlSerializer(typeof(TItem));
            using(var streamReader = new StringReader(row["xmlData"].ToString())) {
                try {
                    item = (TItem)ser.Deserialize(streamReader);
                } catch(Exception) {
                    throw;
                }
            }
            return item;
        }
        static string GetExistQuery(object row) {
            PropertyInfo[] infos = row.GetType().GetProperties();
            List<string> existQuery = new List<string>();
            foreach(var info in infos) {
                if(info.GetValue(row)==null){
                    continue;
                }
                existQuery.Add($"{info.Name} = \"{info.GetValue(row)?.ToString().Replace("\'", "\'\'").Replace("&", "&amp;")}\" ");
            }
            existQuery[0] = $"N'/{row.GetType().Name}[" + existQuery[0];
            existQuery[existQuery.Count - 1] = existQuery[existQuery.Count - 1] + "]\'";
            return String.Join(" and ", existQuery.ToArray());
        }
        public override void Serialize(object data) {
            CheckStorage();
            foreach(var row in (TCollection)data) {
                string test = $"BEGIN IF NOT EXISTS (select * from {GetTableName()} as A where A.xmlData.exist({GetExistQuery(row)}) = 1 ) " +
                    $" Begin INSERT INTO {GetTableName()} (ObjectType, XmlData) VALUES('{typeof(TItem).Name}' , N\'{RowToXml(row)}\' )  END END";
                Execute(test);
            }
        }
        public override TCollection DeserializeLocal() {
            TItem item = new TItem();
            TCollection collection = DataToCollection($"SELECT * FROM {GetTableName()} Where ObjectType='{typeof(TItem).Name}'");
            return collection;
        }
        public override bool CheckStorage() {
            try {
                using(SqlConnection connection = new SqlConnection(StorageLocation)) {
                    connection.Open();
                }
            } catch(Exception) {
                throw;
            }
            try {
                int rowsCount = (int)Execute($"SELECT COUNT(*) FROM {GetTableName()}").Rows[0].ItemArray[0];
                return true;
            } catch(Exception) {
                CreateTable();
                return false;
            }
        }
        private void CreateTable() {
            TItem item = new TItem();
            Execute($"Create TABLE {GetTableName()} (ObjectType nvarchar(50), XmlData xml)");
        }
    }
}