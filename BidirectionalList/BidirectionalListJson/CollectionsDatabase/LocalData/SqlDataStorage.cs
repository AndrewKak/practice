﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DatabaseLocal {
    internal class SqlDataStorage<TCollection, TItem> : LocalDataStorage<TCollection> where TCollection : ICollection<TItem>, new() where TItem : new() {
        public SqlDataStorage(string location) : base(location) {
        }
        DataTable Execute(string commandText) {
            DataTable dataTable = new DataTable();
            using(SqlConnection connection = new SqlConnection(StorageLocation)) {
                try {
                    using(SqlCommand command = new SqlCommand()) {
                        command.Connection = connection;
                        command.CommandType = CommandType.Text;
                        command.CommandText = commandText;
                        connection.Open();
                        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                        dataAdapter.Fill(dataTable);
                        return dataTable;
                    }
                } catch(SqlException) {
                    return dataTable;
                }
            }
        }
        static object GetPropertyValue(object any, string propertyName) {
            PropertyInfo[] info = any.GetType().GetProperties();
            foreach(var item in info) {
                if(propertyName == item.Name) {
                    if(item.GetValue(any) == null) {
                        return string.Empty;
                    } else if(!item.GetValue(any).ToString().Contains("'")) {
                        return item.GetValue(any);
                    } else {
                        var builder = new StringBuilder();
                        foreach(var letter in item.GetValue(any).ToString()) {
                            builder.Append(letter);
                            if(letter.Equals('\'')) {
                                builder.Append(letter);
                            }
                        }
                        return builder.ToString();
                    }
                }
            }
            throw new Exception();
        }
        static string GetValues(object item) {
            //SqlCommand command;
            //command.Parameters.AddWithValue("@p1", p.GetValue());
            //stringValues = "@p1, p2, p3";
            PropertyInfo[] propertyInfos = item.GetType().GetProperties();
            string stringValues = String.Join(",", propertyInfos.Select(p => "\'" + GetPropertyValue(item, p.Name) + "\'").ToArray());
            return stringValues;
        }
        static string GetColumnsTypes() {
            TItem item = new TItem();
            PropertyInfo[] propertyInfos = item.GetType().GetProperties();
            string stringNames = String.Join(",", propertyInfos.Select(p => p.Name + " varchar(50)").ToArray());
            return stringNames;
        }
        static string GetColumns() {
            TItem item = new TItem();
            PropertyInfo[] propertyInfos = item.GetType().GetProperties();
            string stringNames = String.Join(",", propertyInfos.Select(p => p.Name).ToArray());
            return stringNames;
        }
        static string GetTableName() {
            TItem item = new TItem();
            return item.GetType().Name+"s";
        }
        static void SetProperty(object any, string propertyName, object value) {
            PropertyInfo[] info = any.GetType().GetProperties();
            foreach(var item in info) {
                if(propertyName == item.Name) {
                    if(value == System.DBNull.Value) {
                        break;
                    } else {
                        item.SetValue(any, value);
                        break;
                    }
                }
            }
        }
        TCollection DataToCollection(string commandText) {
            TCollection collection = new TCollection();
            var dataTable = Execute(commandText);
            foreach(DataRow row in dataTable.Rows) {
                TItem item = new TItem();
                PropertyInfo[] info = row.GetType().GetProperties();
                DataColumnCollection columnCollection = row.Table.Columns;
                for(int i = 0; i < columnCollection.Count; i++) {
                    string name = columnCollection[i].ColumnName;
                }
                collection.Add(item);
            }
            return collection;
        }
        public override void Serialize(object data) {
            CheckStorage();
            TItem item = new TItem();
            Execute($"Truncate table {GetTableName()}");
            foreach(var row in (TCollection)data) {
                Execute($"INSERT INTO {GetTableName()} ({GetColumns()}) VALUES({GetValues(row)});");
            }
        }
        public override TCollection DeserializeLocal() {
            TItem item = new TItem();
            TCollection collection = DataToCollection($"SELECT * FROM {GetTableName()}");
            return collection;
        }
        public override bool CheckStorage() {
            try {
                using(SqlConnection connection = new SqlConnection(StorageLocation)) {
                    connection.Open();
                }
            } catch(Exception) {
                throw;
            }
            try {
                int rowsCount = (int)Execute($"SELECT COUNT(*) FROM {GetTableName()}").Rows[0].ItemArray[0];
                return true;
            } catch(Exception) {
                CreateTable();
                return false;
            }
        }
        private void CreateTable() {
            TItem item = new TItem();
            Execute($"Create TABLE {GetTableName()}  ({GetColumnsTypes()})");
        }
    }
}