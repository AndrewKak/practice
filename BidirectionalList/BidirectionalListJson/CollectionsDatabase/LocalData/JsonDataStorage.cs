﻿using Newtonsoft.Json;
using System.IO;
namespace DatabaseLocal {
    internal class JsonDataStorage<TCollection> : LocalDataStorage<TCollection> {
        public JsonDataStorage(string location) : base(location) {
        }
        public override void Serialize(object data) {
            CheckStorage();
            if(File.Exists(StorageLocation)) {
                File.Delete(StorageLocation);
            }
            JsonSerializer jsonSerializer = new JsonSerializer();
            using(var streamWriter = new StreamWriter(StorageLocation)) {
                using(var jsonWriter = new JsonTextWriter(streamWriter)) {
                    jsonSerializer.Serialize(jsonWriter, data);
                }
            }
        }
        public override TCollection DeserializeLocal() {
            CheckStorage();
            if(!File.Exists(StorageLocation)) {
                throw new FileLoadException();
            }
            JsonSerializer jsonSerializer = new JsonSerializer();
            TCollection data;
            using(var streamReader = new StreamReader(StorageLocation)) {
                using(var jsonReader = new JsonTextReader(streamReader)) {
                   data = jsonSerializer.Deserialize<TCollection>(jsonReader);
                }
            }
            return data;
        }
        public override bool CheckStorage() {
            if(!Directory.Exists(Path.GetDirectoryName(StorageLocation))) {
                throw new DirectoryNotFoundException();
            }
            if(File.Exists(StorageLocation)) {
                return true;
            } else {
                return false;
            }
        }
    }
}