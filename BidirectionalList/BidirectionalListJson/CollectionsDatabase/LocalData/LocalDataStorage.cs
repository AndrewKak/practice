﻿namespace DatabaseLocal {
    internal abstract class LocalDataStorage<TCollection> {
        internal string StorageLocation { get; set; }
        public LocalDataStorage(string storageLocation) {
            StorageLocation = storageLocation;
            //CheckStorage();
        }
        public abstract void Serialize(object data);
        public abstract TCollection DeserializeLocal();
        public abstract bool CheckStorage();
    }
}