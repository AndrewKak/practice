﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace DatabaseLocal {
    internal class XmlDataStorage<TCollection> : LocalDataStorage<TCollection> {
        public XmlDataStorage(string location) : base(location) {
        }
        public override void Serialize(object data) {
            CheckStorage();
            if(File.Exists(StorageLocation)) {
                File.Delete(StorageLocation);
            }
            XmlSerializer ser = new XmlSerializer(typeof(TCollection));
            using(var streamWriter = new StreamWriter(StorageLocation)) {
                ser.Serialize(streamWriter, data);
            }
        }
        public override TCollection DeserializeLocal() {
            CheckStorage();
            if(!File.Exists(StorageLocation)) {
                throw new FileLoadException();
            }
            TCollection data;
            XmlSerializer ser = new XmlSerializer(typeof(TCollection));
            using(var streamReader = new StreamReader(StorageLocation)) {
                data = (TCollection)ser.Deserialize(streamReader);
            }
            return data;
        }
        public override bool CheckStorage() {
            if(!Directory.Exists(Path.GetDirectoryName(StorageLocation))) {
                throw new DirectoryNotFoundException();
            }
            if(File.Exists(StorageLocation)) {
                return true;
            } else {
                return false;
            }
        }
    }
}