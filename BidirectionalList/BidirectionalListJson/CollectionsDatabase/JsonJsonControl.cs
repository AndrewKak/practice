﻿using BidirectionalList;
using System;
using System.Collections.Generic;
namespace DatabaseLocal {
    public class JsonJsonControl<TCollection, TItem, TResult> : AbstractDataControl<TCollection, TItem, TResult> where TCollection : ICollection<TItem>, new() where TResult : IContainCollection<TCollection> where TItem : IEquatable<TItem> {
        public JsonJsonControl(string storageLocation, string url) : base() {
            localData = new JsonDataStorage<TCollection>(storageLocation);
            remoteData = new WebData<TResult>(url);
            DatabaseCreator<TCollection, TItem, TResult>.DatabaseCreate(this);
        }
    }
}