﻿using BidirectionalList;
using System;
using System.Collections.Generic;
namespace DatabaseLocal {
    public class JsonSqlControl<TCollection, TItem, TResult> : AbstractDataControl<TCollection, TItem, TResult> where TCollection : ICollection<TItem>, new() where TResult : IContainCollection<TCollection>, new() where TItem : IEquatable<TItem>, new() {
        public JsonSqlControl(string connectionString, string accessString) : base() {
            localData = new SqlDataStorage<TCollection,TItem>(connectionString);
            remoteData = new WebData<TResult>(accessString);
            DatabaseCreator<TCollection, TItem, TResult>.DatabaseCreate(this);
        }
    }
}