﻿using BidirectionalList;
using System;
using System.Collections.Generic;
using System.IO;
namespace DatabaseLocal {
    public abstract class AbstractDataControl<TCollection, TItem, TResult> : ICollectionDatabase<TCollection> where TResult : IContainCollection<TCollection> where TCollection : ICollection<TItem>, new() {
        internal RemoteData<TResult> remoteData;
        internal Collection<TCollection, TItem> collection;
        internal LocalDataStorage<TCollection> localData;
        protected AbstractDataControl() {
            collection = new Collection<TCollection, TItem>();
        }
        public void ReadDatabaseLocal() {
            collection.Results = localData.DeserializeLocal();
        }
        public  void DownloadData() {
            collection.Results = remoteData.DeserializeRemote().GetCollection();
            SaveCollection();
        }
        public void SaveCollection() {
            localData.Serialize(collection.Results);
        }
    }
}