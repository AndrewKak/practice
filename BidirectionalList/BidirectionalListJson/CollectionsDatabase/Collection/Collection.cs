﻿using System.Collections.Generic;
namespace DatabaseLocal {
    public class Collection<TCollection, TItem>  where TCollection : ICollection<TItem>, new() { 
        public TCollection Results { get; internal set; }
        public Collection(){
            Results = new TCollection();
        }
        internal void CreateEmptyCollection() {
            Results = new TCollection();
        }
    }
}