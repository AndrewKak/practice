﻿using BidirectionalList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLocal {
    public class AnyTypeSqlControl<TCollection, TItem, TResult> : AbstractDataControl<TCollection, TItem, TResult> where TCollection : ICollection<TItem>, new() where TResult : ICollection<TItem>, IContainCollection<TCollection>, new() where TItem : IEquatable<TItem>, new() {
        public AnyTypeSqlControl(string storageLocation, string accessString) : base() {
            localData = new SqlAnyTypeStorage<TCollection,TItem>(storageLocation);
            remoteData = new SqlData<TResult, TItem>(accessString);
            DatabaseCreator<TCollection, TItem, TResult>.DatabaseCreate(this);
        }
    }
}