﻿using BidirectionalList;
using System;
using System.Collections.Generic;
namespace DatabaseLocal {
    public class SqlXmlControl<TCollection, TItem, TResult> : AbstractDataControl<TCollection, TItem, TResult> where TCollection : ICollection<TItem>, new() where TResult : ICollection<TItem>, IContainCollection<TCollection>, new() where TItem : IEquatable<TItem>,new() {
        public SqlXmlControl(string storageLocation, string accessString) : base() {
            localData = new XmlDataStorage<TCollection>(storageLocation);
            remoteData = new SqlData<TResult, TItem>(accessString);
            DatabaseCreator<TCollection, TItem, TResult>.DatabaseCreate(this);
        }
    }
}