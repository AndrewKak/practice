﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
[assembly: InternalsVisibleTo("SqlTests")]
namespace DatabaseLocal {
    internal class SqlData<TResult, TItem> : RemoteData<TResult> where TResult : ICollection<TItem>, new() where TItem : new() {
        public SqlData(string connection) : base(connection) {
        }
        private DataTable Select(string commandText) {
            DataTable dataTable = new DataTable();
            using(SqlConnection connection = new SqlConnection(RemoteAddress)) {
                using(SqlCommand command = new SqlCommand()) {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = commandText;
                    connection.Open();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                    dataAdapter.Fill(dataTable);
                    return dataTable;
                }
            }
        }
        static void SetProperty(object any, string propertyName, object value) {
            PropertyInfo[] info = any.GetType().GetProperties();
            foreach(var item in info) {
                if(propertyName == item.Name) {
                    if(value.ToString() != string.Empty) {
                        item.SetValue(any, value);
                    }
                }
            }
        }
        static string GetTableName() {
            TItem item = new TItem();
            return item.GetType().Name + "s";
        }
        private TResult ExcuteObject(string commandText) {
            TResult items = new TResult();
            var dataTable = Select(commandText);
            foreach(DataRow row in dataTable.Rows) {
                TItem item = new TItem();
                PropertyInfo[] info = row.GetType().GetProperties();
                DataColumnCollection columnCollection = row.Table.Columns;
                for(int i = 0; i < columnCollection.Count; i++) {
                    string name = columnCollection[i].ColumnName;
                    if(row[name].ToString()!=string.Empty){
                        SetProperty(item, name, row[name]);
                    }
                }
                items.Add(item);
            }
            return items;
        }
        public override TResult DeserializeRemote() {
            CheckAddress();
            return ExcuteObject($"SELECT * FROM {GetTableName()};");
        }
        public override void CheckAddress() {
            try {
                using(SqlConnection connection = new SqlConnection(RemoteAddress)) {
                    connection.Open();
                }
            } catch(Exception) {
                throw;
            }
        }
    }
}