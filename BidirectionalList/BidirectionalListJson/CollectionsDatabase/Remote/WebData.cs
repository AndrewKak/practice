﻿using Newtonsoft.Json;
using System;
using System.Net;
namespace DatabaseLocal {
    internal class WebData<TResult> : RemoteData<TResult> {
        public WebData(string location) : base(location) {
        }
        private string DownloadData() {
            CheckAddress();
            string database = new WebClient().DownloadString(RemoteAddress);
            return database;
        }
        public override TResult DeserializeRemote() {
            try {
              var  data = JsonConvert.DeserializeObject<TResult>(DownloadData());
                return data;
            } catch(Exception) {
                throw new JsonSerializationException();
            }
        }
        public override void CheckAddress() {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(RemoteAddress);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if(!RemoteAddress.Contains(".json")||!(Uri.TryCreate(RemoteAddress, UriKind.Absolute, out Uri uriResult))||(response.StatusCode!= HttpStatusCode.OK)) {
                throw new UriFormatException("Not working url");
            }
            response.Close();
        }
    }
}