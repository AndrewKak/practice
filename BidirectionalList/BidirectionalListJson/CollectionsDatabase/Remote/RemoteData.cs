﻿using System;
using System.Configuration;

namespace DatabaseLocal {
    internal abstract class RemoteData<TResult> {
        public string RemoteAddress { get; internal set; }
        protected RemoteData(string remoteAddress) {
            RemoteAddress = remoteAddress;
            CheckAddress();
        }
        public abstract TResult DeserializeRemote();
        public abstract void CheckAddress(); 
    }
}