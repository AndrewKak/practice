﻿using BidirectionalList;
using System.Collections.Generic;
namespace DatabaseLocal {
    public class DatabaseEditor<TCollection, TItem, TResult> where TResult : IContainCollection<TCollection> where TCollection : ICollection<TItem>, new() {
        private AbstractDataControl<TCollection, TItem, TResult> CollectionDatabase { get; set; }
       // private TCollection Results { get; set; }
        public DatabaseEditor(AbstractDataControl<TCollection, TItem, TResult> collectionDatabase) {
            CollectionDatabase = collectionDatabase;
           // Results = CollectionDatabase.collection.Results;
        }
        public void Add(TItem item) {
            CollectionDatabase.collection.Results.Add(item);
        }
        public bool Remove(TItem item) {
            bool isRemove = CollectionDatabase.collection.Results.Remove(item); ;
            return isRemove;
        }
        public int Length() {
            return CollectionDatabase.collection.Results.Count;
        }
        public TCollection GetCurrentCollection() {
            return CollectionDatabase.collection.Results;
        }
        public void ChangeStorage(string location) {
            CollectionDatabase.localData.StorageLocation = location;
        }
        public void Serialize(object data) {
            CollectionDatabase.localData.Serialize( data);
        }
        public TCollection DeserializeLocal( ) {
           return CollectionDatabase.localData.DeserializeLocal();
        }
        public void CheckStorage() {
             CollectionDatabase.localData.CheckStorage();
        }

        public void ChangeRemoteSource(string source) {
            CollectionDatabase.remoteData.RemoteAddress = source;
        }
        public TResult DeserializeRemote() {
            return CollectionDatabase.remoteData.DeserializeRemote();
        }
        public void CheckRemoteSource() {
            CollectionDatabase.remoteData.CheckAddress();
        }
    }
}