﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
namespace DatabaseLocal {
    public class StringConnection  {
        public string ConnectionString{get;set;}
        public StringConnection(string stringName) {
            ConnectionString = ConfigurationManager.ConnectionStrings[stringName].ConnectionString;
        }
    }
}
