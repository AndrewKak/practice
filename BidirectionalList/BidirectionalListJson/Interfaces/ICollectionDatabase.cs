﻿namespace DatabaseLocal {
    public interface ICollectionDatabase<TCollection> {
        void ReadDatabaseLocal();
        void DownloadData();
        void SaveCollection();
    }
}